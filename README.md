# Analysis scripts for calibration

## Setup

If using the standalone setup, setup environment using the g4env.sh script as usual.
Then update the following variables in the enviroment

```
export PYTHONDIR=/afs/cern.ch/sw/lcg/external/Python/2.7.3/$ARCH/
export PYTHONPATH=$PYTHONPATH:$ROOTSYS/lib
export LD_LIBRARY_PATH=$ROOTSYS/lib:$PYTHONDIR/lib:`pwd`/lib:$LD_LIBRARY_PATH
```

If using CMSSW do instead
```
cmsrel CMSSW_9_3_2
cd CMSSW_9_3_2/src
cmsenv
git clone git@github.com:CMS-HGCAL/reco-ntuples.git RecoNtuples
git clone git@github.com:nadjieh/ntuple-tools.git
git clone https://gitlab.cern.ch/psilva/HGCStandaloneAnalysis.git
scram b
```

Notice that to executables under src will only compile in the standalone setup case.

## Converting standalone outputs to reco-ntuples
For details on reco-ntuples see https://github.com/CMS-HGCAL/reco-ntuples.
The excecutable `src/prepareRecoNtuple.cpp` does the translation of the necessary info.
It takes as input a directory with standalone SIM/DIGI files and does the necessary matching
to loop synchronously over the two. The file names to be used should also be passed in the configuration file.
An example configuration can be found in `cfg/reco-ntuples.cfg`
Example:
```
./bin/prepareRecoNtuple --cfg cfg/reco-ntuples.cfg
```
If you don't want to be editing the cfg all the time you can use the following wrapper.
The option `-n` will make it loop over chunks of 10 files
```
python test/loopPrepareTreeOverDir.py -e prepareRecoNtuple\
       --out results/reco-ntuple.root \
       --sim HGcal__version63_model2_BOFF --reco DigiIC3_200u_version63_model2_BOFF \
       -n 10 /eos/cms/store/cmst3/group/hgcal/Geant4/gitFHGranularity/gamma;
```


## Analyze the rec hit profiles

Prepare the control distributions of the RecHit energies in the silicon and scintillator in a ROOT file with the quantiles
of the spectra which can be used for global or software compensation a posteriori. The second script uses the quantiles to
```
python test/prepareRecHitProfiles.py       -i data/ --tag Geant4_pi- -o calib_v63/
python test/createCompensationTree.py      -i data/ --tag Geant4_pi- -q calib_v63/Geant4_pi-rechitquantiles_trivial.root 
python test/optimizeCompensationWeights.py -i data/ --tag Geant4_pi- --swcomp data/swcomp --et 100 --eta 2.2
```

## Preparing the workspaces for fitting

Some scripts to prepare the workspaces for the fit

* create the summary trees starting from standalone
```
./bin/prepareCalibrationTree --cfg cfg/calibrationTree.cfg
```
To enable looping over several files you can use the following wrapper instead as shown below.
After all is done you can hadd the chunks.
```
for i in gamma pi-; do 
    for v in 63 64 65 66 630 640 650 660; do
        outDir=/eos/cms/store/cmst3/user/psilva/HGCal/FHGranularity/version${v};
        mkdir -p ${outDir};
        python test/loopPrepareTreeOverDir.py \
               --out ${outDir}/Geant4_${i}.root \
               --sim HGcal__version${v}_model2_BOFF --reco DigiIC3_200u_version${v}_model2_BOFF \
               -n 10 /eos/cms/store/cmst3/group/hgcal/Geant4/gitFHGranularity/${i} --jobs 8;
    done
done
```

* convert to workspace for fitting purposes, based on the output of standalone summary trees
```
for i in gamma pi-; do
    for v in 63 64 65 66 630 640 650 660; do
        inDir=/eos/cms/store/cmst3/user/psilva/HGCal/FHGranularity/version${v}
        python test/createCompensationTree.py  -i ${inDir} --tag Geant4_${i} --jobs 8
        python test/convert2workspace.py       -i ${inDir} --match Geant4_${i}
     done
done
```

* convert to workspace for fitting purposes, starting from the CMSSW reco-ntuples
```
#create pickle summaries (takes a while better submit it on condor)
indir=/eos/cms/store/cmst3/group/hgcal/CMG_studies/Production/FlatRandomPtGunProducer_SinglePiPt2Eta1p6_2p8_Fall17DR-NoPUFEVT_clange_20180129/NTUP/
outdir=/eos/cms/store/cmst3/user/psilva/HGCal/Fall17DR-NoPU/
python test/convert2workspace.py --cmssw -i ${indir}  -o ${outdir}

#process pickle summaries
indir=${outdir}
python test/convert2workspace.py --cmssw -i ${indir}  -o ${outdir}
````

## Calibration

The calibration sequence starting from the electromagnetic scale calibration with photons
to the pion calibration (pi/e + residual energy sharing) is summarized in

```
sh test/steerCalibrationSequence.sh
```

which one can modify according to the needs.


* generate a descoped workspace 
Descoping is hardcoded in convert2workspace.py. Change it according to your needs then run
Once the workspace is available, the calibration can be resumed as described above.
```
python test/convert2workspace.py -i /eos/cms/store/cmst3/user/psilva/HGCal/version63/Geant4_pi-.root -t descopedFH --descopeMode 0
```

* shower profile analysis
```
python showShowerProfile.py -i /eos/cms/store/cmst3/user/psilva/HGCal/gamma.root
```
