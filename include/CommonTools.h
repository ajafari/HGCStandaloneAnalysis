#ifndef _commontools_h_
#define _commontools_h_

#include "TString.h"
#include "TFile.h"
#include "TChain.h"

#include <iostream>
#include <string>

//
//OPENS FILES AND CHECKS HOW MANY EVENTS ARE AVAILABLE
//
Int_t checkFileIntegrity(TString url,TString tname)
{
  Int_t nentries(0);
  TFile *fIn=TFile::Open(url);
  if(fIn)
    {
      if(!fIn->IsZombie()) 
        {
          TTree *t=(TTree *)fIn->Get(tname);
          if(t) nentries=t->GetEntriesFast();
        }      
      fIn->Close();
    }
  return nentries;
}

//
// INPUT: CHECK THAT FOR EACH DIGI THERE IS A CORRESPONDING SIM FILE 
// AND MAKE THE TWO CHAINS FRIENDS SO THEY CAN BE READ SIMULTANEOUSLY
//
std::pair<TChain *,TChain *>getEventChain(std::string recoTreeName,
                                          std::string recoTag,
                                          std::string simTreeName,
                                          std::string simTag,
                                          std::string inDir,
                                          unsigned pStartInputs,
                                          unsigned pNinputs,
                                          unsigned debug)
{
  TChain *reco = new TChain(recoTreeName.c_str());
  TChain *sim = new TChain(simTreeName.c_str());
  TSystemDirectory dir(inDir.c_str(),inDir.c_str());
  TList *files = dir.GetListOfFiles();
  TSystemFile *file;
  TIter next(files);
  unsigned ngood(0);
  unsigned ninputs(0);
  while ((file=(TSystemFile*)next())) {
    if( file->IsDirectory() ) continue;
    TString fname( file->GetName() );
    TString simfname(fname);
    simfname=simfname.ReplaceAll(recoTag,simTag);
    if( !fname.Contains(".root") ) continue;
    if( !fname.Contains(recoTag.c_str()) ) continue;
    Int_t nreco=checkFileIntegrity(inDir+"/"+fname,recoTreeName);
    Int_t nsim=checkFileIntegrity(inDir+"/"+simfname,simTreeName);
    if(nreco>0 && nsim==nreco)
      {
        if(ngood>=pStartInputs)
          {
            reco->AddFile(inDir+"/"+fname);        
            sim->AddFile(inDir+"/"+simfname);
            ninputs+=1;
          }
        ngood+=1;
      }
    else
      {
        std::cout << "[Warn] will discard inputs in " << fname << "/" << simfname << std::endl;
      }
    if(debug>0)
      {
        std::cout << fname << " " << simfname << std::endl
                  << "\t" << nreco << " reco / " << nsim  << " sim" << std::endl;
      }
    
    //check if enough files were collected already
    if(pNinputs>0 && ninputs>= pNinputs) break;
  }
  reco->AddFriend(sim);
  std::cout << "Total events available:" << reco->GetEntries() << std::endl;

  std::pair<TChain *,TChain *> evChain(reco,sim);
  return evChain;
}

//uses first file to read HGCSSInfo
HGCSSDetector &getDetector(TChain *sim) 
{
  TFile *curF=sim->GetFile();
  HGCSSInfo *info=(HGCSSInfo*)curF->Get("Info");
  std::cout << curF->GetName() << std::endl;
  assert(info);
  unsigned versionNumber = info->version();
  unsigned model         = info->model();
  std::cout << "Starting detector v" << versionNumber << " model" << model << std::endl;
  HGCSSDetector &myDetector=theDetector();
  myDetector.buildDetector(info);
  return myDetector;
}

#endif
