import optparse
import os,sys
import ROOT
import numpy as np
from myProgressBar import *
import pickle
import array

COLORS=[ROOT.TColor.GetColor(ci) for ci in ['#33a02c','#1f78b4','#e31a1c','#ff7f00','#cab2d6','#a6cee3']]

def addHeader(c,addLegend=True):
    """aux function to stamp the canvas with a common header"""
    if addLegend: 
        leg=c.BuildLegend(0.6,0.9,0.92,0.7)
        leg.SetTextFont(42)
        leg.SetTextSize(0.03)
        leg.SetFillStyle(0)
        leg.SetFillColor(0)
        leg.SetBorderSize(0)
    tex=ROOT.TLatex()
    tex.SetTextFont(42)
    tex.SetTextSize(0.04)
    tex.SetNDC()
    tex.DrawLatex(0.15,0.96,'#bf{CMS} #it{simulation preliminary}')
    tex.DrawLatex(0.72,0.96,'HGCAL Geant4')


def prepareShowerProfiles(opt):
    """loops over the tree, selects events in range and shows the transverse dispersion plot"""

    #open file
    fIn=ROOT.TFile.Open(opt.input)

    #x0 weights
    h=fIn.Get('x0')
    wgts=[ h.GetBinContent(i+1) for i in xrange(0,opt.maxLay) ]
    wgtSum=sum(wgts)
    wgts=[ w/wgtSum for w in wgts ]

    #define the baseline histograms
    metrics={
        'dRho':('#rho [mm]',(0.,1000.),400),
        'dR'  :('#DeltaR',(0.,0.5),50)
        }
    dx={}
    incProfile_T={}
    diffProfile_T={}
    for mkey in metrics:
        metricTitle,metricRange,nbins=metrics[mkey]
        dx[mkey]=(metricRange[1]-metricRange[0])/float(nbins)
        incProfile_T[mkey]     = ROOT.TProfile('incProfile_%s_T'%mkey,
                                              'Inclusive;%s;Energy [MIP] / (%3.2f)'%(metricTitle,dx[mkey]),
                                              nbins,metricRange[0],metricRange[1])
        diffProfile_T[mkey]    = [ incProfile_T[mkey].Clone('diffProfile_%s_T_%d'%(mkey,i+1)) for i in xrange(0,opt.maxLay) ]
    incProfile_L     = ROOT.TProfile('incProfile_L',';Layer number;Energy [MIP]',opt.maxLay,1,opt.maxLay+1)
    incFracProfile_L = ROOT.TProfile('incFracProfile_L',';Layer number;Energy fraction',opt.maxLay,1,opt.maxLay+1)
    
    #loop over hits and fill histograms per (ET,eta)
    hits=fIn.Get('hits')
    nentries=hits.GetEntriesFast()
    allIncProfile_T={}
    allDiffProfile_T={}
    allIncProfile_L={}
    allIncFracProfile_L={}
    for i in xrange(0,nentries):
        hits.GetEntry(i)
        if i%10==0 : drawProgressBar(float(i)/float(nentries))
        
        #instantiate histograms if needed
        et=int(hits.genEt)
        eta=int(10*hits.genEta)
        key=(et,eta)
        if not key in allIncProfile_T:
            pfix='_%3.2f_%3.2f'%key
            title='(E_{T},#eta)=(%3.2f,%3.2f)'%key
            allIncProfile_T[key]={}
            allDiffProfile_T[key]={}
            allIncProfile_L[key]={}
            allIncFracProfile_L[key]={}
            for mkey in metrics:
                allIncProfile_T[key][mkey]=incProfile_T[mkey].Clone( incProfile_T[mkey].GetName()+pfix )
                allIncProfile_T[key][mkey].SetTitle(title)
                allDiffProfile_T[key][mkey]=[]
                for ilay in xrange(0,len(diffProfile_T[mkey])):
                    p=diffProfile_T[mkey][ilay]
                    allDiffProfile_T[key][mkey].append( p.Clone(p.GetName()+pfix) )
                    allDiffProfile_T[key][mkey][-1].SetTitle(title+' Layer=%d'%(ilay+1))
            allIncProfile_L[key]=incProfile_L.Clone(incProfile_L.GetName()+pfix)
            allIncProfile_L[key].SetTitle(title)
            allIncFracProfile_L[key]=incFracProfile_L.Clone(incFracProfile_L.GetName()+pfix)
            allIncFracProfile_L[key].SetTitle(title)
        
        #count energy for th different metrics
        enCounters={}
        for mkey in metrics:
            enCounters[mkey]=np.zeros((opt.maxLay,nbins))
        for j in xrange(0,hits.nhits):

            p=hits.hit_lay[j]
            if p>=opt.maxLay: continue
            en=hits.hit_en[j]
            for mkey in metrics:
                d=getattr(hits,'hit_%s'%mkey)[j]
                ibin=min( metrics[mkey][2]-1, 
                          int((d-metrics[mkey][1][0])/dx[mkey]) )
                enCounters[mkey][p][ibin]+=en

        for mkey in metrics:

            #fill transverse profiles
            totalShowerEn=0
            for ibin in xrange(0,nbins):
                totalEn=0
                x=(ibin+0.5)*dx[mkey]+metrics[mkey][1][0]
                for ilay in xrange(0,opt.maxLay):
                    ien=enCounters[mkey][ilay][ibin] #*wgts[ilay]
                    totalEn+=ien
                    totalShowerEn+=ien
                    allDiffProfile_T[key][mkey][ilay].Fill(x,ien)
                allIncProfile_T[key][mkey].Fill(x,totalEn)

            #fill longitudinal profiles
            if mkey!='dRho': continue
            for ilay in xrange(0,opt.maxLay):
                ien=sum(enCounters[mkey][ilay])
                allIncProfile_L[key].Fill(ilay+1,ien)
                allIncFracProfile_L[key].Fill(ilay+1,ien/totalShowerEn)
            
    #dump results to a pickle file
    pck=opt.input.replace('.root','.pck')
    with open(pck, 'w') as cachefile:
        pickle.dump(allIncProfile_L,     cachefile, pickle.HIGHEST_PROTOCOL)
        pickle.dump(allIncFracProfile_L, cachefile, pickle.HIGHEST_PROTOCOL)
        pickle.dump(allIncProfile_T,     cachefile, pickle.HIGHEST_PROTOCOL)
        pickle.dump(allDiffProfile_T,    cachefile, pickle.HIGHEST_PROTOCOL)

    fIn.Close()
    return pck

def showShowerProfiles(opt):
    """opens the pickle file and displays the shower profiles"""
    with open(opt.input, 'r') as cachefile:
        allIncProfile_L=pickle.load(cachefile)
        allIncFracProfile_L=pickle.load(cachefile)
        allIncProfile_T=pickle.load(cachefile)
        allDiffProfile_T=pickle.load(cachefile)

    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetOptTitle(0)

    c=ROOT.TCanvas('c','c',500,500)
    c.SetTopMargin(0.06)
    c.SetLeftMargin(0.15)
    c.SetBottomMargin(0.1)
    
    #differential transverse profile
    r90Summary={}
    c.SetRightMargin(0.1)
    probSum = array.array('d', [0.68,0.90])
    q = array.array('d', [0.0,0.0])
    for mkey,yran in [
        ('dRho',(0,1000.)),
        ('dR',(0,0.5))
        ]:
        for key in allDiffProfile_T:
            
            title='(E_{T},#eta)=(%3.0f,%3.2f)'%(key[0],float(key[1])/10.)
            totalEn=allIncProfile_L[key].Integral()           

            #differential containment
            gr=[ROOT.TGraph(),ROOT.TGraph()]
            grEnFrac=[[],[]]
            prof=allDiffProfile_T[key][mkey]
            for i in xrange(0,len(prof)):
                prof[i].GetQuantiles(2, q, probSum)
                enInLay=prof[i].Integral()
                for j in xrange(0,2):
                    gr[j].SetPoint(i,i+1,q[j])
                    grEnFrac[j].append(100*enInLay*probSum[j]/totalEn)

            maxLay=gr[0].GetXaxis().GetXmax()
            frame=ROOT.TH2F('frame','frame',int(maxLay),0,maxLay,10,yran[0],yran[1])
            for j in xrange(0,2):
                x,y=ROOT.Double(0),ROOT.Double(0)
                for i in xrange(0,len(grEnFrac[j])):
                    gr[j].GetPoint(i,x,y)
                    frame.Fill(x,y,grEnFrac[j][i])

            frame.Draw('colz')
            frame.GetZaxis().SetRangeUser(0,10)
            frame.GetYaxis().SetTitleSize(0.04)
            frame.GetYaxis().SetLabelSize(0.04)
            frame.GetXaxis().SetTitleSize(0.04)
            frame.GetXaxis().SetLabelSize(0.04)
            frame.GetYaxis().SetTitleOffset(1.7)
            frame.GetYaxis().SetTitle( prof[0].GetXaxis().GetTitle() )
            frame.GetXaxis().SetTitle( 'Layer number' ) 

            for j in xrange(0,2):
                gr[j].GetYaxis().SetRangeUser(yran[0],yran[1])
                gr[j].SetLineWidth(2)
                gr[j].SetLineColor(COLORS[j])
                gr[j].Draw('l')
 
            #inclusive containment
            allIncProfile_T[key][mkey].GetQuantiles(2,q,probSum)
            r68,r90=q[0],q[1]
            
            eta=key[1]
            if not eta in r90Summary: r90Summary[eta]={}
            if not mkey in r90Summary[eta]: r90Summary[eta][mkey]=[]
            r90Summary[eta][mkey].append((key[0],r90))

            l=ROOT.TLine()
            l.SetLineStyle(9)
            l.SetLineColor(ROOT.kGray)
            l.DrawLine(0,r68,maxLay,r68)
            l.DrawLine(0,r90,maxLay,r90)
            tex=ROOT.TLatex()
            tex.SetTextFont(42)
            tex.SetTextSize(0.035)
            tex.DrawLatex(2,r68+0.01*yran[1],'#it{R_{68%}}')
            tex.DrawLatex(2,r90+0.01*yran[1],'#it{R_{90%}}')
            tex.SetNDC()
            tex.DrawLatex(0.17,0.9,title)
            tex.SetTextAngle(90)
            tex.DrawLatex(0.93,0.67,'% shower energy')

            addHeader(c,addLegend=False)
            c.Modified()
            c.Update()    
            for ext in ['png','pdf']: c.SaveAs('%s_profileT_%3.0f_%3.2f.%s'%(mkey,key[0],key[1],ext))        
            frame.Delete()

    #inclusive profiles
    peakSummary={}
    c.SetRightMargin(0.02)
    for profs,mkey,name in [
        (allIncProfile_L,None,'profileL'),
        (allIncFracProfile_L,None,'fracprofileL'),
        (allIncProfile_T,'dRho','dRho_profileT'),
        (allIncProfile_T,'dR','dR_profileT'),
        ]:
        
        keysByEta={}
        for key in profs:
            eta=key[1]
            if not eta in keysByEta: keysByEta[eta]=[]
            keysByEta[eta].append(key)

        for eta in keysByEta:
            c.Clear()
    
            ip=0
            ymax=0
            frame=None
            for key in keysByEta[eta]:

                #some may be missing
                if not key in profs: continue

                if mkey: h=profs[key][mkey]
                else   : h=profs[key]

                if name=='profileL':
                    if not eta in peakSummary: peakSummary[eta]=[]
                    peakSummary[eta].append( (key[0],h.GetMaximumBin()) )

                h.GetYaxis().SetTitleSize(0.04)
                h.GetYaxis().SetLabelSize(0.04)
                h.GetXaxis().SetTitleSize(0.04)
                h.GetXaxis().SetLabelSize(0.04)
                h.GetYaxis().SetTitleOffset(1.7)
                h.SetLineWidth(2)            
                h.SetLineColor(COLORS[ip%len(COLORS)])
                h.SetLineStyle(1+ip/len(COLORS))
                h.Draw('hist' if ip==0 else 'histsame')
                if ip==0: frame=h
                ymax=max(ymax,h.GetMaximum()*1.1)
                ip+=1

            #adjust y-scale
            frame.GetYaxis().SetRangeUser(0,ymax)
            addHeader(c=c,addLegend=True)
            c.Modified()
            c.Update()        
            for ext in ['png','pdf']: c.SaveAs('%s_%3.2f.%s'%(name,eta,ext))
    
    #show final summaries
    for summary,mkey,name,ytitle,xtitle in [
        (peakSummary, None,  'peakEvol',     'Layer for maximum', 'E_{T} [GeV]'),
        (r90Summary, 'dRho', 'dRho_r90Evol', 'R_{90%} [mm]',      'E_{T} [GeV]'),
        (r90Summary, 'dR',   'dR_r90Evol',   '#DeltaR_{90%}',     'E_{T} [GeV]'),
        ]:
        
        c.Clear()
        ip=0
        ymin,ymax=1e7,0
        grs=[]
        frame=None
        allEtas=sorted(summary.keys())
        for eta in allEtas:
            
            results=summary[eta]
            if mkey:
                if not eta in summary: continue
                print summary[eta]
                results=summary[eta][mkey]

            grs.append( ROOT.TGraph() )
            grs[-1].SetTitle('#eta=%3.2f'%(float(eta)/10.))
            grs[-1].SetLineWidth(2)
            grs[-1].SetFillStyle(0)
            grs[-1].SetMarkerColor(COLORS[ip%len(COLORS)])
            grs[-1].SetLineColor(COLORS[ip%len(COLORS)])
            grs[-1].SetLineStyle(1+ip/len(COLORS))

            for et,val in results:
                n=grs[-1].GetN()
                grs[-1].SetPoint(n,et,val)
                ymax=max(val,ymax)
                ymin=min(val,ymin)
            grs[-1].Sort()
            grs[-1].Draw('al' if ip==0 else 'l')
            grs[-1].GetXaxis().SetTitle(xtitle)
            grs[-1].GetYaxis().SetTitle(ytitle)
            ip+=1
            
        grs[0].GetYaxis().SetTitleSize(0.04)
        grs[0].GetYaxis().SetLabelSize(0.04)
        grs[0].GetXaxis().SetTitleSize(0.04)
        grs[0].GetXaxis().SetLabelSize(0.04)
        grs[0].GetYaxis().SetTitleOffset(1.7)
        grs[0].GetYaxis().SetRangeUser(ymin*0.9,ymax*1.1)
        addHeader(c=c,addLegend=True)
        c.Modified()
        c.Update()        
        for ext in ['png','pdf']: 
            c.SaveAs('%s.%s'%(name,ext))
        

def main():
    """wrapper to be used from command line"""
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('-i', '--in',       dest='input',  help='Input file',                      default='results/gamma_test.root')    
    parser.add_option(      '--maxLay',   dest='maxLay', help='max.layer',                       default=28, type=int) 
    (opt, args) = parser.parse_args()
    if '.root' in opt.input : opt.input=prepareShowerProfiles(opt)
    if '.pck'  in opt.input : opt.input=showShowerProfiles(opt)
        

if __name__ == "__main__":
    sys.exit(main())
