import ROOT
import sys

ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)
ROOT.gStyle.SetPalette(42)

c=ROOT.TCanvas('c','c',500,500)
c.SetTopMargin(0.06)
c.SetLeftMargin(0.15)
c.SetBottomMargin(0.1)
c.SetRightMargin(0.02)

fIn=ROOT.TFile.Open(sys.argv[1])
hits=fIn.Get('hits')
hits.SetMarkerStyle(20)

for i in xrange(0,hits.GetEntriesFast()):
    hits.GetEntry(i)
    if hits.genEn>50*1e3: continue
    if abs(hits.genEta)>2.4 or abs(hits.genEta)<1.6 : continue

    hits.Draw("hit_x:hit_y:hit_z:hit_en >> gr","(Entry$==%d && hit_en>1)"%i,"z")

    gr=c.GetPrimitive('gr')
    gr.GetXaxis().SetTitle('z [mm]')
    gr.GetYaxis().SetTitle('y [mm]')
    gr.GetZaxis().SetTitle('x [mm]')
    gr.GetXaxis().SetTitleOffset(1.8)
    gr.GetYaxis().SetTitleOffset(2.2)
    gr.GetZaxis().SetTitleOffset(1.8)

    tex=ROOT.TLatex()
    tex.SetTextFont(42)
    tex.SetTextSize(0.04)
    tex.SetNDC()
    tex.DrawLatex(0.15,0.96,'#bf{CMS} #it{simulation preliminary}')
    tex.DrawLatex(0.72,0.96,'HGCAL Geant4')
    tex.SetTextSize(0.035)
    tex.DrawLatex(0.15,0.9,'E=%3.0f GeV #eta=%3.2f'%(hits.genEn*1e-3,hits.genEta))
    
    c.Modified()
    c.Update()
    raw_input()

