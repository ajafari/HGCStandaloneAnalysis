#!/usr/bin/env python

import ROOT
import io,os,sys
import optparse
from rootTools import showGraphCollection,getGrComparison

def getPlotsFrom(url):
    fIn=ROOT.TFile.Open(url)
    mg=fIn.Get('c').GetListOfPrimitives().At(1)
    fIn.Close()
    plots={}
    for gr in mg.GetListOfGraphs():
        plots[gr.GetTitle()]=gr
    return mg.GetXaxis().GetTitle(),mg.GetYaxis().GetTitle(),plots

def getFunctionRatio(f1,f2,xmin,xmax,step):
    x=xmin
    gr=ROOT.TGraph()
    gr.SetName(f1.GetName()+'_ratio')
    while x<xmax:
        ratio=f2.Eval(x)/f1.Eval(x)
        gr.SetPoint(gr.GetN(),x,f2.Eval(x)/f1.Eval(x))
        x+=step
    return gr

def showComparison(inputs,out,xtit,ytit,yran,logx,noratio,ytitle):

    colors=['#8c510a','#35978f','#4393c3','#b2182b','#4d4d4d']

    rctr=0
    for r in inputs[0][2]:
        rctr+=1
        ratioColl={}
        ratioCollp2p={}
        startidx=0 if noratio else 1
        for i in xrange(startidx,len(inputs)):

            if not r in inputs[i][2]: continue
            
            #fit based
            try:
                gr1func=inputs[0][2][r].GetListOfFunctions().At(0)
                gr1func.SetName('f_0_%s'%r)
                gr2func=inputs[i][2][r].GetListOfFunctions().At(0)
                gr2func.SetName('f_%d_%s'%(i,r))
                xmin,xmax=inputs[0][2][r].GetX()[0],inputs[0][2][r].GetX()[inputs[0][2][r].GetN()-1]
                step=(xmax-xmin)/100.
                if noratio:
                    ratioColl[i]=gr2func.Clone()
                    ratioColl[i].SetName( '%s_param%d'%(inputs[i][2][r].GetName(),i) )
                else:
                    ratioColl[i]=getFunctionRatio(f1=gr1func,f2=gr2func,xmin=xmin,xmax=xmax,step=step)
                    ratioColl[i].SetName( '%s_ratio%d'%(inputs[i][2][r].GetName(),i) )
                ratioColl[i].SetTitle(inputs[i][0])
                ratioColl[i].SetLineWidth(2)
                ratioColl[i].SetLineColor(ROOT.TColor.GetColor(colors[i]))
                ratioColl[i].SetMarkerColor(ROOT.TColor.GetColor(colors[i]))
                ratioColl[i].Sort()
            except:
                pass

            #point to point based
            if noratio:
                ratioCollp2p[i]=inputs[i][2][r].Clone('%s_%d'%(inputs[i][2][r].GetName(),i))
            else:
                ratioCollp2p[i]=getGrComparison(gr1=inputs[0][2][r],gr2=inputs[i][2][r],ratio=True)
            ratioCollp2p[i].SetTitle(inputs[i][0])
            ratioCollp2p[i].SetMarkerStyle(inputs[i][2][r].GetMarkerStyle())
            ratioCollp2p[i].SetLineWidth(2)
            ratioCollp2p[i].SetLineColor(ROOT.TColor.GetColor(colors[i]))
            ratioCollp2p[i].SetMarkerColor(ROOT.TColor.GetColor(colors[i]))
            ratioCollp2p[i].Sort()

        showGraphCollection(grColl=ratioColl if len(ratioColl)>0 else ratioCollp2p,
                            outName='%s_%d'%(out,rctr),
                            xtitle=xtit,
                            ytitle=ytitle,
                            yran=yran,
                            logx=logx,
                            grid=True,
                            rightLeg=True,
                            drawOpt='l',
                            extraInfo=[r],
                            grColl2=ratioCollp2p if len(ratioColl)>0 else None)


def main():
    
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('-i',      '--in' ,      
                      dest='inputs',
                      help='Input files title2:file2,title2:file2,... [%default]',                                     
                      default=None)
    parser.add_option('--yran' ,      
                      dest='yran',
                      help='y range [%default]',
                      default=(0.8,2))
    parser.add_option('--logx' ,      
                      dest='logx',
                      default=False,
                      action='store_true')
    parser.add_option('--noratio' ,      
                      dest='noratio',
                      default=False,
                      action='store_true')
    parser.add_option('--ytitle' ,      
                      dest='ytitle',
                      default='Ratio')                      
    parser.add_option('-o',      '--out' ,      
                      dest='output',
                      help='output files [%default]',                                     
                      default=None)
    (opt, args) = parser.parse_args()

    yran=[ float(y) for y in opt.yran.split(',') ]

    inputs=[x.split(':') for x in opt.inputs.split(',')]
    for i in xrange(0,len(inputs)):
        title,url=inputs[i]
        xtit,ytit,plots=getPlotsFrom(url=url)
        inputs[i].append(plots)

    showComparison(inputs=inputs,out=opt.output,xtit=xtit,ytit=ytit,yran=yran,logx=opt.logx,noratio=opt.noratio,ytitle=opt.ytitle)


if __name__ == "__main__":
    sys.exit(main())
