#!/usr/bin/env python

import ROOT
import io,os,sys
import optparse
import commands
import pickle
import numpy as np
from array import array
from rootTools import initROOT,getQuantiles,getEffSigma,buildTH2Profile
from absE import doABSTypeCalibration
from piOverE import doPiOverETypeCalibration
from swCompensationTools import parametrizeCglob,parametrizeCglobVersusEn,evalCglobCorrection
from calibrationPoints import *

def evalAtCategory(grDict,x,cat,evalFunc=None,allowRange=(0.5,2.0)):
    """evaluate function or graph corresponding to a given category, at a given value of x"""
    #if function is stored use it, otherwise interpolate from spline
    sf=1.0
    try:
        f=grDict[cat].GetFunction(evalFunc)
        sf=f.Eval(x)
    except:
        sf=grDict[cat].Eval(x)
    return min(allowRange[1],max(allowRange[0],sf))

def parametrizeResResponse(h):
    """parametrizes the residual response as function of the energy shared"""
    gr=buildTH2Profile(h)
    pol2=ROOT.TF1('pol2','[0]*pow(x,2)+[1]*x+[2]',0,1)
    pol2.SetParLimits(2,-0.1,0.1)
    pol2.SetParLimits(1,-0.2,0.2)
    pol2.SetParLimits(0,-0.2,0.2)
    print "In parametrizeResResponse method ---- "
    gr.Fit('pol2','MEX0RQ+','same',0,1)
    return gr

def parametrizeResResponseVersusEn(resResponseParamMap):
    """parametrizes the evolution as function of the energy of the residual parameterization""" 
    grColl={}
    for ieta in xrange(0,len(ETA_PTS)):
        grColl[ieta]=[]
        for i in xrange(0,3):
            grColl[ieta].append( ROOT.TGraphErrors() )
            grColl[ieta][-1].SetMarkerStyle(20)
            grColl[ieta][-1].SetName('resresp_param%d_%d_evol'%(i,ieta))
            for key in resResponseParamMap:                                
                if key[1]!=ieta: continue
                en=ET_PTS[key[0]]*ROOT.TMath.CosH(ETA_PTS[key[1]])
                try:
                    f=resResponseParamMap[key].GetFunction('pol2')
                    p,perr=f.GetParameter(i),f.GetParError(i)
                    np=grColl[ieta][-1].GetN()
                    grColl[ieta][-1].SetPoint(np,en,p)
                    grColl[ieta][-1].SetPointError(np,0,perr)
                except Exception as e:
                    print e
                    pass
            grColl[ieta][-1].Sort()        
            p=ROOT.TF1('resresp_p%d_%d'%(i,ieta),
                       'x>[3] ? [0]*log(x)+[1] : [2]*log(x)+([0]-[2])*log([3])+[1]',1,1e4)
            p.SetParName(0,'a')
            p.SetParName(1,'b')
            p.SetParName(2,'c')
            p.SetParName(3,'x_{0}')
            p.SetParLimits(3,15,50)
            print "In parametrizeResResponseVersusEn method wiith i = %s" %i
            grColl[ieta][-1].Fit(p,'MRWQ+','',2.0,1.0e4)
    return grColl

def evalResRespCorrection(x,a,b,c):
    """evaluates the parametrized residual response correction"""
    resresp=a*(x**2)+b*x+c
    return 1.0/(1.0+resresp)

def runCalibrationFits(opt,applyCalibSeq,cglob_var,tag,outDir,elimIdx):
    """runs the calibration fits for every simulated point and dumps a pickle file with the summary of the results"""
    cglob_var+=("_%s"%elimIdx)
    #readout all calibration/fit results files
    absCalib={}
    pioeCalib={}
    resCalib={}
    gcCalib=None
    for key in applyCalibSeq:
        url=applyCalibSeq[key]    
        if 'ABS' in key:
            absCalib[key]={}
            with open(url,'r') as cachefile:
                centralCalib=pickle.load(cachefile)
                etaDependentCalib=pickle.load(cachefile)            
            for v in centralCalib:
                absCalib[key][v]=(centralCalib[v],centralCalib[v].Clone(),etaDependentCalib[v])
        if 'PIOE' in key:
            with open(url,'r') as cachefile:
                pickle.load(cachefile)
                pioeCalib[key]=pickle.load(cachefile)
        if 'RES' in key:
            with open(url,'r') as cachefile:
                resCalib=pickle.load(cachefile)
        if 'GC' in key:
            with open(url,'r') as cachefile:
                gcCalib=pickle.load(cachefile)

    print 'Will apply the following absolute calibration'
    print absCalib
    print 'Will apply the following pi/e correction'
    print pioeCalib
    print 'Will apply the following residual energy share correction'
    print resCalib
    if gcCalib:
        print 'Parameters for global compensation taken from',gcCalib,'for',cglob_var
    else:
        print 'No global compensation will be applied'

    #control histograms
    hFracEt={}
    hFracEta={}   
    for det in ['si','sci']:
        hFracEt[det]=ROOT.TH2F('%sfracet'%det,';E_{T} [GeV];E_{sci}/E_{total};',len(ET_PTS)-1,array('d',ET_PTS),50,0,1)
        hFracEt[det].SetDirectory(0)
        hFracEta[det]=ROOT.TH2F('%sfraceta'%det,';Pseudo-rapidity;E_{sci}/E_{total};',len(ETA_PTS)-1,array('d',ETA_PTS),50,0,1)
        hFracEta[det].SetDirectory(0)
    resResponse={}
    hCglob={}
    for key in [(p1,p2) for p1 in xrange(0,len(ET_PTS)) for p2 in xrange(0,len(ETA_PTS))]:
        p1,p2=key
        title='E_{T}=%3.1f GeV |#eta|=%3.1f'%(ET_PTS[p1],ETA_PTS[p2])
        binEnShare = [0,0.05,0.1,0.15,0.2,0.3,0.5,0.75,1.0]
        resResponse[key]=ROOT.TH2F('resresp_%d_%d' %(key),'%s;E(BH)/E;E / E(gen) - 1;Events'%title,8,array('d',binEnShare),50,-0.25,0.25)
        resResponse[key].SetDirectory(0)
        hCglob[key]=ROOT.TH2F('cglob_%d_%d' %(key),'%s;C_{glob};E / E(gen) - 1;'%title,50,0,2,50,-0.25,0.25)
        hCglob[key].SetDirectory(0)

    #read inputs from file
    fIn=ROOT.TFile.Open(opt.input)
    print "Input file %s" %opt.input
    w=fIn.Get('w')

    #create a new dataset with the final variables for calibration
    dataVars=ROOT.RooArgSet(w.var('en'), w.var('eta'))
    w.factory('iet[0,1000]')
    dataVars.add(w.var('iet'))
    w.factory('ieta[0,1000]')
    dataVars.add(w.var('ieta'))
    varList=opt.varList.split(',')
    for v in varList: 
        w.factory('en_%s[0,9.0e+12]'%v)
        dataVars.add( w.var('en_%s'%v) )
    getattr(w,'import')( ROOT.RooDataSet('data_calib','data_calib',dataVars) )

    #iterate over events to fill calibrated energy dataset
    for ientry in xrange(0,w.data('data').numEntries()):
    #for ientry in xrange(0,5):
        entryVars=w.data('data').get(ientry)

        #MC truth for this point
        et,eta=entryVars.find('et').getVal(),entryVars.find('eta').getVal()
        iet,ieta=matchToSimPoint(et,eta,getValue=False)        
        if iet is None : continue
        if ieta is None: continue
        et,eta=ET_PTS[iet],ETA_PTS[ieta]
        en=et*ROOT.TMath.CosH(eta)

        veto=False
        varVals={}
        for v in varList:            
            en_si= entryVars.find('en_si_{0}'.format(v)).getVal()
            en_sci = entryVars.find('en_sci_{0}'.format(v)).getVal()
            totalRaw=(en_sci+en_si)
            #print "en_si = %d, en_sci = %d" %(en_si,en_sci)        
            #a min. quantity
            if totalRaw<1e-6 : 
                veto=True
                continue 

            #for calibration of Si standalone
            f_sci=en_sci/totalRaw
            if opt.vetoSci>0 and f_sci>opt.vetoSci:
                veto=True
                continue
        
            #calibrate energy in the scintillator
            if 'ABS_Sci' in absCalib:
                key='ABS_Sci'
                vAbs=v.replace('descoped','avg')
                pcen=absCalib[key][vAbs][1].GetParameter(0)
                shift=absCalib[key][vAbs][2][0].Eval(eta)                
                en_sci *= pcen*shift            
                key='PIOE_Sci'
                vPIOE=v.replace('descoped','avg')
                if key in pioeCalib and vPIOE in pioeCalib[key]:
                    en_sci *= evalAtCategory(grDict=pioeCalib[key][vPIOE],x=en_sci,cat=ieta)

            #calibrate energy in the silicon
            if 'ABS_Si' in absCalib:
                key='ABS_Si'                
                vAbs=v.replace('descoped','avg')
                pcen=absCalib[key][vAbs][1].GetParameter(0)
                shift=absCalib[key][vAbs][2][0].Eval(eta)
                en_si*=pcen*shift                
                key='PIOE_Si'
                if key in pioeCalib and v in pioeCalib[key]:
                    en_si *= evalAtCategory(grDict=pioeCalib[key][v],x=en_si,cat=ieta)
 
            #total linearized energy
            caliben=en_si+en_sci

            #residuals as function of shared energy
            enShare=en_sci/caliben
            if v=='dedx_avg' :
                if len(resCalib):
                    caliben *= evalResRespCorrection(enShare,
                                                     resCalib[ieta][0].Eval(caliben),
                                                     resCalib[ieta][1].Eval(caliben),
                                                     resCalib[ieta][2].Eval(caliben))


            #global compensation is applied once all responses have been linearized
            cglob=1.0
            try:                
                cglob=entryVars.find(cglob_var).getVal()
            except:
                pass
            if(cglob == -1): cglob=1.0
            if gcCalib:
                caliben *= evalCglobCorrection(cglob,
                                               gcCalib[0].Eval(caliben),
                                               gcCalib[1].Eval(caliben),
                                               gcCalib[2].Eval(caliben),
                                               gcCalib[3].Eval(caliben))

            #final absolute calibration (if needed)
            if 'ABS' in absCalib: 
                key='ABS'
                for i in xrange(0,absCalib[key][v][0].GetNpar()):
                    pcen=absCalib[key][v][1].GetParameter(i)
                    shift=absCalib[key][v][2][i].Eval(eta)
                    absCalib[key][v][0].SetParameter(i,pcen*shift)
                caliben = absCalib[key][v][0].Eval(caliben)            
            
            #all done with calibration
            varVals[v]=float(caliben)
            
            #monitor energy sharing
            if 'dedx_avg' in v:                
                hFracEt['sci'].Fill(et,en_sci/caliben)
                hFracEta['sci'].Fill(eta,en_sci/caliben)                
                hFracEt['si'].Fill(et,en_si/caliben)
                hFracEta['si'].Fill(eta,en_si/caliben)
                #print "resResponse Name: %s" %resResponse[(iet,ieta)].GetName()
                #print "enShare: %d, caliben/en-1.0: %d" %(enShare,caliben/en-1.0)
                resResponse[(iet,ieta)].Fill(enShare,caliben/en-1.0)
                if(cglob != -1):
                    hCglob[(iet,ieta)].Fill(cglob,caliben/en-1.0)

        if veto: continue
        
        #add entry to the dataset with calibrated energies
        newEntry=ROOT.RooArgSet()
        w.var('en').setVal(en)
        newEntry.add( w.var('en') )
        w.var('eta').setVal(eta)
        newEntry.add( w.var('eta') )   
        w.var('iet').setVal(iet)
        newEntry.add( w.var('iet') )
        w.var('ieta').setVal(ieta)
        newEntry.add( w.var('ieta') )
        for v in varList:
            w.var('en_%s'%v).setVal(varVals[v])
            newEntry.add( w.var('en_%s'%v) )
        w.data('data_calib').add(newEntry)

    #run fits
    fitResults={}
    for iet,ieta in [(p1,p2) for p1 in xrange(0,len(ET_PTS)) for p2 in xrange(0,len(ETA_PTS))]:

        #check if point is to be used
        et,eta = ET_PTS[iet],ETA_PTS[ieta]
        if eta<opt.minEta or eta>opt.maxEta: 
            print 'Skipping',ipt,' eta=',eta,' is out of range'
            continue
        en=et*ROOT.TMath.CosH(eta)

        #project data
        data=w.data('data_calib').reduce('iet==%d && ieta==%d'%(iet,ieta))
        nEntries=data.numEntries()

        #require some minimum number of simulated events
        if nEntries<opt.minEntries : continue

        #get quantiles
        dataQ = getQuantiles(data,varList)

        for v in varList:

            #init the PDF and fit range
            fitName            = 'range_%d_%d_%s_%s'%(iet,ieta,v,elimIdx)            
            v_mean, v_sigma    = dataQ[v][2],0.5*(dataQ[v][3]-dataQ[v][1])
            v_sigmaL, v_sigmaR = dataQ[v][2]-dataQ[v][1],dataQ[v][3]-dataQ[v][2]

            #define ranges: adapt in case the distribution is skewed to the left or right
            meanRanges=(v_mean,v_mean-v_sigma,v_mean+v_sigma)
            sigmaRanges=(v_sigma,v_sigma*0.9,v_sigma*1.1)
            alphaRanges=(0.001,-10,10)
            if v_sigmaR/v_sigmaL>1.2:
                meanRanges=(dataQ[v][2],dataQ[v][2]-v_sigmaL*0.5,dataQ[v][2]+v_sigmaL*0.5)
                sigmaRanges=(v_sigmaL,v_sigmaL*0.9,v_sigmaL*1.1)
                alphaRanges=(-0.001,-10,0.)
            if v_sigmaL/v_sigmaR>1.2:
                meanRanges=(dataQ[v][2],dataQ[v][2]-v_sigmaR*0.5,dataQ[v][2]+v_sigmaR*0.5)
                sigmaRanges=(v_sigmaR,v_sigmaR*0.9,v_sigmaR*1.1)
                alphaRanges=(0.001,0,10.)

            w.var('en_'+v).setRange('%s'%fitName,dataQ[v][0],dataQ[v][4])
            
            #init pdf and fit it to data
            w.factory('RooCBShape::resol_{0}(en_{1},mean_{0}[{2},{3},{4}],sigma_{0}[{5},{6},{7}],alpha_{0}[{8},{9},{10}],n_{0}[2,0.1,3])'
                      .format(fitName,v, meanRanges[0],meanRanges[1],meanRanges[2],sigmaRanges[0],sigmaRanges[1],sigmaRanges[2],alphaRanges[0],alphaRanges[1],alphaRanges[2])
                      )
            fres = w.pdf('resol_%s'%fitName).fitTo( data, ROOT.RooFit.Range(fitName), ROOT.RooFit.Save(True) )
            meanFit,  meanFit_error  = w.var('mean_%s'%fitName).getVal(),  w.var('mean_%s'%fitName).getError()
            sigmaFit, sigmaFit_error = w.var('sigma_%s'%fitName).getVal(), w.var('sigma_%s'%fitName).getError()
            corr = fres.correlation(w.var('mean_%s'%fitName),w.var('sigma_%s'%fitName))            
            #low,high=getEffSigma(w.var('en_%s'%v),
            #                     w.pdf('resol_%s'%fitName),
            #                     0,2*dataQ[v][4],
            #                     2e-3*dataQ[v][4],
            #                     2e-4*dataQ[v][4])
            sigmaEff=0.5*(dataQ[v][3]-dataQ[v][1])
            
            #save canvas with fit result
            c=ROOT.TCanvas('c_%s' %fitName,'%s'%fitName,500,500)
            c.SetTopMargin(0.06)
            c.SetLeftMargin(0.15)
            c.SetBottomMargin(0.1)
            c.SetRightMargin(0.02)           
            frame=w.var('en_'+v).frame(dataQ[v][0],dataQ[v][4])
            data.plotOn(frame)
            w.pdf('resol_%s'%fitName).plotOn(frame)
            chisq=frame.chiSquare(4)      

            #save in the fit results if goodness of fit is adequate
            if opt.byQuantiles:
                fitResults[v][(iet,ieta)] = (v_mean,0,v_sigma,0,0)
            else:
                 if chisq<2.0:
                     if not v in fitResults: fitResults[v]={}
                     #fitResults[v][(iet,ieta)] = (meanFit,meanFit_error,sigmaFit,sigmaFit_error,corr)
                     fitResults[v][(iet,ieta)] = (meanFit,meanFit_error,sigmaEff,sigmaFit_error,corr)
      
            frame.Draw()
            frame.GetXaxis().SetTitleSize(0.05)
            frame.GetYaxis().SetTitleSize(0.05)

            #represent quantiles
            l=ROOT.TLine()
            l.SetLineColor(ROOT.kGray)
            for q in [1,2,3]:
                l.DrawLine(dataQ[v][q],0,dataQ[v][q],frame.GetMaximum())

            tex=ROOT.TLatex()
            tex.SetTextFont(42)
            tex.SetTextSize(0.04)
            tex.SetNDC()
            tex.DrawLatex(0.15,0.96,'#bf{CMS} #it{simulation preliminary}')
            tex.DrawLatex(0.72,0.96,'HGCAL Geant4')
            tex.SetTextSize(0.035)
            tex.DrawLatex(0.18,0.90,'E_{T}=%3.0f GeV'%et)
            tex.DrawLatex(0.18,0.85,'|#eta|=%3.2f'%eta)
            tex.DrawLatex(0.18,0.80,'#chi^{2}/ndof = %3.1f'%chisq)
            tex.DrawLatex(0.72,0.90,'E/E_{truth} = %3.2f'%(meanFit/en))
            tex.DrawLatex(0.72,0.85,'#sigma_{E}/E = %3.3f'%(sigmaFit/meanFit))
            tex.DrawLatex(0.72,0.80,'#sigma^{eff}_{E}/E = %3.3f'%(sigmaEff/meanFit))
            c.SaveAs(os.path.join(outDir,'%s%s.png'%(fitName,tag)))
            #c.Delete()

    #prepare the final list of control plots, including
    # - fitting the residuals of the response as function of the shared energy between Si and SCi
    # - fitting the evolution of Cglob for global compensation
    plotList  = [(hFracEt[x],  buildTH2Profile(hFracEt[x]), 'colz', False, True) for x in hFracEt]
    plotList += [(hFracEta[x], buildTH2Profile(hFracEt[x]), 'colz', False, True) for x in hFracEta]
    print "resResponse is %s" %resResponse 
    resResponseGr={}
    resResponseGr=dict(map((lambda x: (x,parametrizeResResponse(resResponse[x])) if x in resResponse else (x,None)),resResponse))
    plotList += [(resResponse[x],resResponseGr[x],'colz',False,False) for x in resResponse if resResponse[x].Integral()>0]
    resResponseParamVsEnGr=parametrizeResResponseVersusEn(resResponseGr)
    for i in resResponseParamVsEnGr:
        for gr in resResponseParamVsEnGr[i]:
            plotList.append( (gr,None,'ap',True,False) )
    cglobParamVsEnGr=None
    hCglobGr=dict(map((lambda x: (x,parametrizeCglob(hCglob[x])) if x in hCglob else (x,None)),hCglob))
    plotList += [(hCglob[x],hCglobGr[x],'colz',False,False) for x in hCglob if hCglob[x].Integral()>0]
    cglobParamVsEnGr=parametrizeCglobVersusEn(hCglobGr)
    plotList += [(x,None,'ap',True,False) for x in cglobParamVsEnGr if x.GetN()>0]

    #draw control plots
    c=ROOT.TCanvas('c_%s'%elimIdx,'c',500,500)
    c.SetTopMargin(0.06)
    c.SetLeftMargin(0.15)
    c.SetBottomMargin(0.15)
    for h,gr,drawOpt,logx,logz in plotList:

        c.SetRightMargin(0.15 if 'col' in drawOpt else 0.04)
        c.SetLogx(logx)
        c.SetLogz(logz)  

        h.Draw(drawOpt)
        ffunc=h.GetListOfFunctions().At(0)
        if gr: 
            gr.Draw('p')
            ffunc=gr.GetListOfFunctions().At(0)

        #attempt to show fit result parameters
        try:
            fitRes=ROOT.TLatex()
            fitRes.SetTextFont(42)
            fitRes.SetTextSize(0.03)
            fitRes.SetNDC()
            fitRes.DrawLatex(0.18,0.9,ffunc.GetExpFormula().Data())
            for i in xrange(0,ffunc.GetNpar()):
                fitRes.DrawLatex(0.18,0.85-i*0.05,'%s=%3.3f'%(ffunc.GetParName(i),ffunc.GetParameter(i)))
        except:
            pass

        tex=ROOT.TLatex()
        tex.SetTextFont(42)
        tex.SetTextSize(0.04)
        tex.SetNDC()
        tex.DrawLatex(0.15,0.96,'#bf{CMS} #it{simulation preliminary}')
        tex.DrawLatex(0.72,0.96,'HGCAL Geant4')
        c.Modified()
        c.Update()
        c.SaveAs(os.path.join(outDir,'%s%s%s.png'%(h.GetName(),tag,elimIdx)))
        c.SaveAs(os.path.join(outDir,'%s%s%s.root'%(h.GetName(),tag,elimIdx)))
    #c.Delete()    

    #save residuals as function of energy sharing
    resCalibFile='rescalibresults%s_ABS_%s.pck'%(tag,elimIdx)
    pck=os.path.join(outDir,resCalibFile)
    with open(pck, 'w') as cachefile:
        pickle.dump(resResponseParamVsEnGr,cachefile, pickle.HIGHEST_PROTOCOL)

    #save cglob parameterizations
    if cglobParamVsEnGr:
        cglobCalibFile='cglobcalib%s_ABS_%s.pck'%(tag,elimIdx)
        pck=os.path.join(outDir,cglobCalibFile)
        cglobParamFuncs=[x.GetListOfFunctions().At(0) for x in cglobParamVsEnGr]
        with open(pck, 'w') as cachefile:
            pickle.dump(cglobParamFuncs,cachefile, pickle.HIGHEST_PROTOCOL)

    return fitResults

"""
steer 
"""
def main():
    
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('-i',      '--in' ,      
                      dest='input',
                      help='Input file [%default]',                                     
                      default=None)
    parser.add_option('--minEta' ,      
                      dest='minEta',
                      type=float,
                      help='minimum pseudo-rapidity [%default]',                                     
                      default=1.4)
    parser.add_option('--maxEta' ,      
                      dest='maxEta',
                      type=float,
                      help='maximum pseudo-rapidity [%default]',                                     
                      default=3.0)
    parser.add_option('--minE' ,      
                      dest='minE',
                      type=float,
                      help='minimum E used in calibration fit [%default]',                                     
                      default=0)
    parser.add_option('--maxE' ,      
                      dest='maxE',
                      type=float,
                      help='maximum E used in calibration fit [%default]',                                     
                      default=300)
    parser.add_option('--minEntries',
                      dest='minEntries',
                      type=int,
                      help='min. number of events to fit [%default]',
                      default=200)
    parser.add_option('-v', '--var' ,     
                      dest='varList',  
                      help='CSV list of variables to use as energy estimator [%default]',  
                      default='dedx_avg') #dedx,dedx_avg,trivial')
    parser.add_option('-c', '--calib',    
                      dest='calib',  
                      help='Calibration to perform (e.g. ABS) [%default]',  default='ABS')
    parser.add_option('--applyCalib',    
                      dest='applyCalibSeq',  
                      help='Calibration sequence to apply (e.g. ABS). If found inside directory where workspace is, it will be used [%default]',  default=None)
    parser.add_option('--out',    
                      dest='outDir',  
                      help='output directory for plots and pickle files with calibration [%default]',  default='calib')
    parser.add_option('-b', '--batch' ,    
                      dest='batch',  
                      help='Batch mode [%default]',  action='store_true', default=False)
    parser.add_option('--byQuantiles' ,    
                      dest='byQuantiles',  
                      help='Calibrate by quantiles [%default]',  action='store_true', default=False)
    parser.add_option('--cglob',
                      dest='cglob_var',
                      help='cglob variable [%default]',  default='cglob_fh')
    parser.add_option('--vetoSci',
                      dest='vetoSci',
                      help='veto events with x% total raw energy in scintillator [%default]',  default=-1, type=float)
    parser.add_option('--noiseResol',
                      dest='noiseResol',
                      help='use resolution with noise term [%default]',  default=False, action='store_true')
    parser.add_option('--elimId',
                      dest='elimIdx',
                      help='use the cglob computred with this elim id, default is elim = [%default]',  default=5, type=int)
    (opt, args) = parser.parse_args()

    #check inputs
    if opt.input is None:
        parser.print_help()
        sys.exit(1)
        
    #define the calibration sequence
    applyCalibSeq={}
    if  opt.applyCalibSeq:
        for key in  opt.applyCalibSeq.split(','):
            tag,url=key.split(':')
            applyCalibSeq[tag]=url
    tag=''.join(applyCalibSeq.keys())
    print 'Will apply the following calibration items',applyCalibSeq
    print 'Will derive the following calibration',opt.calib
    print 'Result will be tagged as',tag

    initROOT(opt.batch)
    os.system('mkdir -p %s'%opt.outDir)

   
    #run resolution fits
    fitResults = runCalibrationFits(opt=opt,applyCalibSeq=applyCalibSeq,cglob_var=opt.cglob_var,tag=tag,outDir=opt.outDir,elimIdx=opt.elimIdx)
    #analyze individual fit results to derive pi/e and absolute calibrations
    if opt.calib=='ABS'  : doABSTypeCalibration(opt=opt,fitResults=fitResults,tag=tag,outDir=opt.outDir,noiseResol=opt.noiseResol,elimIdx=opt.elimIdx)
    if opt.calib=='PIOE' : doPiOverETypeCalibration(opt=opt,fitResults=fitResults,tag=tag,outDir=opt.outDir,elimIdx=opt.elimIdx)

if __name__ == "__main__":
    sys.exit(main())
