import ROOT
import os
import sys
import numpy as np
from array import array
from myProgressBar import *
from layerWeightingSchemes import configureLayerWeights
from swCompensationTools import getCglob
from prepareRecHitProfiles import inputConfig,getMatchedKey
from sklearn import linear_model
import numpy as np

def runLinearRegression(X,y):
    """wrapper to run linear regression with sci-kit"""
    reg = linear_model.LinearRegression(fit_intercept=False)
    reg.fit (X,y)
    #reg = linear_model.Lasso(alpha = 0.5,fit_intercept=False)
    #reg.fit(X,y)
    #reg = linear_model.Ridge (alpha = 0.5,fit_intercept=False)
    #reg.fit(X,y)

    y_pred = reg.predict(X)
    relRes=y_pred/y[0]-1
    # The coefficients
    print('Coefficients (linear): \n', reg.coef_)
    p=np.percentile(relRes, [16,50,84])
    print('Bias=%3.3f SigmaEff=%3.3f'%(p[1],p[2]-p[0]))

    return relRes

def buildDataset(opt,args):
    """filters out the relevant events and creates the dataset to be optimized"""

    #make trees be friends
    urlList=[os.path.join(opt.input,x) for x in os.listdir(opt.input) if opt.tag in x]
    hits=ROOT.TChain('hits')
    swcomp=ROOT.TChain('swcomp')
    for url in urlList:
        hits.AddFile(url)
        swcomp.AddFile(os.path.join(opt.swcomp,os.path.basename(url)))
    hits.AddFriend(swcomp)

    X,y,s=[],[],[]
    nentries=hits.GetEntries()
    if opt.maxEvts>0 : nentries=min(nentries,opt.maxEvts)
    print('Hits chain has %d events'%nentries)
    keyToMatch=getMatchedKey(opt.et,opt.eta)
    for i in xrange(0,nentries):
        hits.GetEntry(i)
        key=getMatchedKey(hits.genEt,hits.genEta)
        if keyToMatch[0] and keyToMatch[0]!=key[0]: continue
        if keyToMatch[1] and keyToMatch[1]!=key[1]: continue
        if i%50==0 : drawProgressBar(float(i)/float(nentries))
        y.append( key[0]*ROOT.TMath.CosH(key[1]) )
        ientry=[]
        enSums=[0,0,0]
        for sen in hits.en_q_ee  :
            ientry.append(sen)
            enSums[0]+=sen
        for sen in hits.en_q_fh  :
            ientry.append(sen)
            enSums[1]+=sen
        for sen in hits.en_q_sci :
            ientry.append(sen)
            enSums[2]+=sen
        X.append(ientry)
        #spectator variables
        s.append( [hits.cglob_ee,hits.cglob_fh,hits.cglob_sci] )
    return np.array(X),np.array(y),np.array(s)


def main():
    """wrapper to be used from command line"""

    (opt, args) = inputConfig(argList=[(u'--swcomp',),(u'--et',),(u'--eta',)],
                              kwargList=[{'dest':'swcomp','help':'trees with energy sums for software compensation [%default]', 'default':None},
                                         {'dest':'et',    'help':'this transverse energy [%default]', 'type':float,             'default':None},
                                         {'dest':'eta',   'help':'this pseudo-rapidity [%default]',   'type':float,             'default':None}])

    X,y,s=buildDataset(opt,args)
    relRes=runLinearRegression(X,y)
    import matplotlib.pyplot as plt
    for i in xrange(0,3):
        plt.hist2d(relRes, s[:,i], bins=40)
        plt.colorbar()
        plt.show()

if __name__ == "__main__":
    sys.exit(main())
