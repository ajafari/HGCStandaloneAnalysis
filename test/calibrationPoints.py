# this is used to avoid rounding errors of floats in python

ET_PTS=[2,5,7,10,15,25,35,50,100,200]
ETA_PTS=[1.6,1.8,2.0,2.2]
SIM_PTS=[(p1, p2) for p1 in ET_PTS for p2 in ETA_PTS]

def matchToSimPoint(genEt,genEta,getValue=True,maxDeta=0.05,maxDEt=0.1):
    """match to closest simulated point"""
    matchedEt  = min(enumerate(ET_PTS),  key=lambda x: abs(x[1]-genEt))
    matchedEta = min(enumerate(ETA_PTS), key=lambda x: abs(x[1]-genEta))
    if abs(matchedEt[1]-genEt)>maxDEt    : matchedEt=(None,None)
    if abs(matchedEta[1]-genEta)>maxDeta : matchedEta=(None,None)
    return (matchedEt[getValue],matchedEta[getValue])
