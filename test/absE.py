import ROOT
import os
import pickle
from rootTools import showGraphCollection,getGrComparison
from calibrationPoints import *

def doABSTypeCalibration(opt,fitResults,tag,outDir,elimIdx,noiseResol=False):
    """runs a linear calibration based on a set of fit results"""
    
    calibResults={}

    #define the calibration model
    calibModel=ROOT.TF1('calibmodel',"[0]*x",0,1e9)
    biasModel=ROOT.TF1('biasmodel',"[0]-1",0,1e9)
    if noiseResol:
        resolModel=ROOT.TF1('resolmodel','sqrt([0]*[0]/x+[1]*[1]/(x*x)+[2]*[2])',0,1e9) 
        resolModel.SetParameter(0,0.1)
        resolModel.SetParLimits(0,0,1.5)
        resolModel.SetParameter(1,0.0)
        resolModel.SetParLimits(1,0,1.5)
        resolModel.SetParameter(2,0)
        resolModel.SetParLimits(2,0,0.4)
    else:
        resolModel=ROOT.TF1('resolmodel','sqrt([0]*[0]/x+[1]*[1])',0,1e9) 
        resolModel.SetParameter(0,0.1)
        resolModel.SetParLimits(0,0,1.2)
        resolModel.SetParameter(1,0)
        resolModel.SetParLimits(1,0,0.3)
   
    responseGr={}
    biasGr={}
    resolGr={}
    resolEtGr={}
    for v in fitResults:

        #init graphs
        responseGr[v]={}
        biasGr[v]={}
        resolGr[v]={}
        resolEtGr[v]={}
        calibResults[v]={}
    
        #fill response and relative resolution
        maxEn=0
        for iet,ieta in fitResults[v]:

            et,eta=ET_PTS[iet],ETA_PTS[ieta]
            en=et*ROOT.TMath.CosH(eta)          
            maxEn=max(en,maxEn)

            if not ieta in responseGr[v]:
                etaIdx=len(responseGr[v])
                baseName='%d_%s_%s'%(etaIdx,v,elimIdx)
                responseGr[v][ieta]=ROOT.TGraphErrors()
                responseGr[v][ieta].SetMarkerStyle(20+etaIdx)
                responseGr[v][ieta].SetName('response_%s'%baseName)
                responseGr[v][ieta].SetTitle('#eta=%3.2f'%eta)
                calibResults[v][ieta]=calibModel.Clone('calib_%s'%baseName)
                biasGr[v][ieta]=responseGr[v][ieta].Clone('relbias_%s'%baseName)
                resolGr[v][ieta]=responseGr[v][ieta].Clone('relresolution_%s'%baseName)
                resolEtGr[v][ieta]=responseGr[v][ieta].Clone('relresolutionet_%s'%baseName)

            meanFit,meanFit_error,sigmaFit,sigmaFit_error,corr=fitResults[v][(iet,ieta)]
            relResol = sigmaFit/meanFit
            if relResol<0 : 
                print 'Skipping',pt,'with relResol=',relResol
                continue
            relResol_error  =  (meanFit_error/meanFit)**2
            relResol_error += (sigmaFit_error/sigmaFit)**2
            relResol_error += -2*corr*meanFit_error*sigmaFit_error/(meanFit*sigmaFit)
            relResol_error  = relResol*ROOT.TMath.Sqrt(relResol_error)
            
            if relResol_error/relResol>0.20 : continue

            np=responseGr[v][ieta].GetN()
            responseGr[v][ieta].SetPoint(np,en,meanFit)
            responseGr[v][ieta].SetPointError(np,0,meanFit_error)
            biasGr[v][ieta].SetPoint(np,en,meanFit/en-1)
            biasGr[v][ieta].SetPointError(np,0,meanFit_error/en)
            resolGr[v][ieta].SetPoint(np,en,relResol)
            resolGr[v][ieta].SetPointError(np,0,relResol_error)
            resolEtGr[v][ieta].SetPoint(np,et,relResol)
            resolEtGr[v][ieta].SetPointError(np,0,relResol_error)

        toDelete=[]
        for ieta in responseGr[v]:
            responseGr[v][ieta].Sort()
            if responseGr[v][ieta].GetN()<2: toDelete.append(ieta)
            responseGr[v][ieta].Fit(calibModel,'MER+','',0,1000) #opt.minE,opt.maxE)
            calibResults[v][ieta].SetParameter(0,1./calibModel.GetParameter(0))
            calibResults[v][ieta].SetParError(0,calibModel.GetParError(0)/(calibModel.GetParameter(0)**2))
            biasGr[v][ieta].Sort()
            biasGr[v][ieta].Fit(biasModel,'MER+','',0,1e12) #opt.minE,opt.maxE)
            resolGr[v][ieta].Sort()
            resolGr[v][ieta].Fit(resolModel,'MER+','',0,1e12)
            resolEtGr[v][ieta].Sort()
            resolEtGr[v][ieta].Fit(resolModel,'MER+','',0,1e12)

        #remove entries with less than 3 points
        for ieta in toDelete:
            del responseGr[v][ieta]
            del calibResults[v][ieta]
            del biasGr[v][ieta]
            del resolGr[v][ieta]
            del resolEtGr[v][ieta]

    #derive eta dependency of the calibration parameters 
    centralCalib={}
    etaDependentCalib={}
    for v in calibResults:
            
        npar=calibResults[v].values()[0].GetNpar()
        centralCalib[v]=calibModel.Clone('centralcalib_%s_%s'%(v,elimIdx))
        etaDependentCalib[v]={}
        for i in xrange(0,npar) : etaDependentCalib[v][i]=ROOT.TGraphErrors()
        
        for ieta in calibResults[v]:
            for i in xrange(0,npar):
                np=etaDependentCalib[v][i].GetN()
                etaDependentCalib[v][i].SetPoint(np,ETA_PTS[ieta],calibResults[v][ieta].GetParameter(i))
                etaDependentCalib[v][i].SetPointError(np,0,calibResults[v][ieta].GetParError(i))
        
        #normalize to eta=2.0 and add some formatting
        for i in xrange(0,npar):

            kabsnorm=etaDependentCalib[v][i].Eval(2.0)
            centralCalib[v].SetParameter(i,kabsnorm)

            x,y=ROOT.Double(0),ROOT.Double(0)
            for np in xrange(0,etaDependentCalib[v][i].GetN()):
                etaDependentCalib[v][i].GetPoint(np,x,y)
                ey=etaDependentCalib[v][i].GetErrorY(np)
                etaDependentCalib[v][i].SetPoint(np,float(x),float(y)/kabsnorm)
                etaDependentCalib[v][i].SetPointError(np,0,float(ey)/kabsnorm)
            etaDependentCalib[v][i].SetName('%s_eta%d'%(v,i))
            etaDependentCalib[v][i].SetTitle('p%d'%i)
            etaDependentCalib[v][i].Sort()
            etaDependentCalib[v][i].SetMarkerStyle(20+i)
        
    #save calibration and fit results to a pickle file
    calibFile='calibresults%s_ABS_%s.pck'%(tag,elimIdx)
    pck=os.path.join(outDir,calibFile)
    with open(pck, 'w') as cachefile:
        pickle.dump(centralCalib,      cachefile, pickle.HIGHEST_PROTOCOL)
        pickle.dump(etaDependentCalib, cachefile, pickle.HIGHEST_PROTOCOL)
        pickle.dump(calibResults,      cachefile, pickle.HIGHEST_PROTOCOL)        
        pickle.dump(fitResults,        cachefile, pickle.HIGHEST_PROTOCOL)
        pickle.dump(resolGr,           cachefile, pickle.HIGHEST_PROTOCOL)
    print 'Calibration/fit results stored in',calibFile
    

    #do the resolution difference in case descoping
    for grColl,enType in [(resolGr,''), (resolEtGr,'_et')]:
        if 'dedx_avg' in grColl and 'dedx_descoped' in grColl:
            resolDiff={}
            resolRatio={}
            for key in grColl['dedx_avg']:
                resolDiff[key]=getGrComparison(gr1=grColl['dedx_avg'][key],gr2=grColl['dedx_descoped'],ratio=False)
                resolDiff[key].SetName( grColl['dedx_avg'][key].GetName()+'_diff2descoped')
                resolDiff[key].SetTitle( grColl['dedx_avg'][key].GetTitle() )
                resolDiff[key].SetMarkerStyle( grColl['dedx_avg'][key].GetMarkerStyle() )
                resolRatio[key]=getGrComparison(gr1=grColl['dedx_avg'][key],gr2=grColl['dedx_descoped'],ratio=True)
                resolRatio[key].SetName(grColl['dedx_avg'][key].GetName()+'_ratio2descoped')

            showGraphCollection(resolDiff, 
                                os.path.join(outDir,'resolDiff%s_%s%s%s.png'%(enType,v,tag,elimIdx)),
                                'E [GeV]' if enType=='' else 'E_{T} [GeV]',
                                '[(#sigma_{descoped}/E)^{2} - (#sigma/E)^{2}]^{1/2}',
                                None)
            showGraphCollection(resolRatio, 
                                os.path.join(outDir,'resolRatio%s_%s%s%s.png'%(enType,v,tag,elimIdx)),
                                'E [GeV]' if enType=='' else 'E_{T} [GeV]',
                                '#sigma_{descoped} / #sigma',
                                None)


    #draw results to canvas  
    for grColl,name,xtitle,ytitle,func,logx in [
        (responseGr,        'response', 'E [GeV]',         'E_{rec}',       'calibmodel',False),
        (etaDependentCalib, 'etacalib', 'Pseudo-rapidity', 'k_{ABS} / k_{ABS}(#eta=2.0)',       None,False), 
        (biasGr,            'bias',     'E [GeV]',         '( E_{rec} / E - 1 )', 'biasmodel',False),
        (resolGr,           'resol',    'E [GeV]',         '#sigma_{E} / E',  'resolmodel',True),
        (resolEtGr,         'resolet',  'E_{T} [GeV]',     '#sigma_{E} / E',  'resolmodel',True),
        ]:
        for v in grColl: 
            showGraphCollection(grColl=grColl[v], 
                                outName=os.path.join(outDir,'%s_%s%s%s.png'%(name,v,tag,elimIdx)),
                                xtitle=xtitle,
                                ytitle=ytitle,
                                func=func,
                                logx=logx)
