import ROOT
import os
import sys
import numpy as np
from array import array
import optparse
from myProgressBar import *
from layerWeightingSchemes import configureLayerWeights
from swCompensationTools import getCglob
from calibrationPoints import *

def getRecHitSpectrum(urlList,maxEvts=-1,elim=5.0,layerWeights=None):
    """loop over the hits in a file and prepare the recHit spectra summary"""

    #baseline histograms
    cglobH = ROOT.TH1F('cglob',';C_{global};PDF;',  50, 0.5, 2.0)
    enH    = ROOT.TH1F('enh',  ';log (E/MIP);PDF;', 50, -1, 10)

    #final histograms
    histos={}
    def addIfNeeded(key,histos,baseH):
        if key in histos:
            return
        histos[key]={}
        for d in ['ee','fh','sci']:
            histos[key][d]={}
            for h in baseH:
                newName='%s_%s'%(baseH[h].GetName(),d)
                histos[key][d][h]=baseH[h].Clone(newName)
                histos[key][d][h].Sumw2()
                histos[key][d][h].SetDirectory(0)
                histos[key][d][h].SetTitle('E_{T}=%3.0f GeV'%key[0])

    #fill histograms
    hits=ROOT.TChain('hits')
    for url in urlList: hits.AddFile(url)
    nentries=hits.GetEntries()
    if maxEvts>0 : nentries=min(nentries,maxEvts)
    print 'Hits chain has %d events'%nentries
    for i in xrange(0,nentries):
        hits.GetEntry(i)

        if i%50==0 : drawProgressBar(float(i)/float(nentries))

        #sort hit types
        eeHits=[]
        fhHits=[]
        sciHits=[]
        for ih in xrange(0,hits.nhits):
            hen=hits.hit_en[ih]
            if layerWeights:
                lwgt=layerWeights.GetBinContent(hits.hit_lay[ih])
                hen *= lwgt
            if hits.hit_si[ih]:
                if hits.hit_lay[ih]<28:
                    eeHits.append(hen)
                else:
                    fhHits.append(hen)
            else:
                sciHits.append(hen)

        #fill histos
        key=getMatchedKey(hits.genEt,hits.genEta)
        addIfNeeded(key,histos,{'cglob':cglobH,'hen':enH})
        for d,hcoll in [('ee',eeHits),('fh',fhHits),('sci',sciHits)]:
            cglob=getCglob(hcoll=np.array(hcoll),elim=elim)
            histos[key][d]['cglob'].Fill(cglob)
            for hen in hcoll:
                histos[key][d]['hen'].Fill(ROOT.TMath.Log(hen))

    #all done
    return histos


def showDistributions(toPlot,name,title,output):
    """a wrapper for the plotting part"""
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetOptTitle(0)
    ROOT.gStyle.SetPalette(42)

    c=ROOT.TCanvas('c','c',500,500)
    c.SetTopMargin(0.06)
    c.SetLeftMargin(0.12)
    c.SetBottomMargin(0.1)
    c.SetRightMargin(0.05)
    c.SetLogy()

    frame=toPlot[0].Clone()
    frame.Reset('ICE')
    frame.GetYaxis().SetTitleSize(0.04)
    frame.GetXaxis().SetTitleSize(0.04)
    frame.Draw()
    frame.GetYaxis().SetRangeUser(1e-4,1)
    leg=ROOT.TLegend(0.65,0.9,0.95,0.85-0.05*len(toPlot),title)
    leg.SetBorderSize(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.035)
    leg.SetFillStyle(0)
    for hist in toPlot:
        hist.Draw('histsame')
        leg.AddEntry(hist,hist.GetTitle(),'l')
        leg.Draw()

    tex=ROOT.TLatex()
    tex.SetTextFont(42)
    tex.SetTextSize(0.035)
    tex.SetNDC()
    tex.DrawLatex(0.12,0.96,'#bf{CMS} #it{simulation preliminary}')
    tex.DrawLatex(0.78,0.96,'#scale[0.8]{HGCAL Geant4}')
    tex.SetTextSize(0.035)

    c.Modified()
    c.Update()
    c.SaveAs('%s/%s.png'%(output,name))
    frame.Delete()

def inputConfig(argList=[],kwargList=[]):
    """configure how to parse the command line options"""
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('-i', '--in' ,          dest='input',       help='Input directory [%default]',              default=None)
    parser.add_option(      '--tag' ,         dest='tag',         help='file name to match [%default]',           default='Geant4_pi-_')
    parser.add_option('-o', '--out' ,         dest='output',      help='output dir [%default]',                   default=None)
    parser.add_option(      '--elim' ,        dest='elim',        help='elim for Cglobal [%default]',             default=5.0)
    parser.add_option('-w', '--wgt' ,         dest='wgt',         help='weighting scheme (csv list) [%default]',  default='trivial')
    parser.add_option('-s', '--strictWgts' ,  dest='strictWgts',  help='use strict weights',                      default=False,  action='store_true')
    parser.add_option(      '--descopeMode' , dest='descopeMode', help='descope mode',                            default=-1,     type=int)
    parser.add_option(      '--cmssw' ,       dest='cmssw',       help='use cmssw inputs',                        default=False,  action='store_true')
    parser.add_option(      '--maxEvts',      dest='maxEvts',     help='max events to process [%default]',        default=-1,     type=int)
    for i in xrange(0,len(argList)):
        args=argList[i]
        kwargs=kwargList[i]
        parser.add_option(*args,**kwargs)
    return  parser.parse_args()

def main():
    """wrapper to be used from command line"""

    (opt, args) = inputConfig()

    urlList=[os.path.join(opt.input,x) for x in os.listdir(opt.input) if opt.tag in x and not '_swcomp' in x]
    print 'Found %d files in %s to analyze, matching %s'%(len(urlList),opt.input,opt.tag)

    #weigthing scheme
    layerWeights,wgtScheme=configureLayerWeights(opt,urlList[0])

    #get histograms
    histos=getRecHitSpectrum(urlList,maxEvts=opt.maxEvts,elim=opt.elim,layerWeights=layerWeights)

    os.system('mkdir -p %s'%opt.output)

    #prepare output with quantile histograms
    qHistos=[]
    nq = 10
    xq = array('d', [0.] * nq)
    for i in xrange(nq): xq[i]=float(i+1)/nq
    yq = array('d', [0.] * nq)
    outF=ROOT.TFile.Open('%s/%srechitquantiles_%s.root'%(opt.output,opt.tag,wgtScheme),'RECREATE')

    #show plots and fill quantile distributions
    colours=[ROOT.kBlack, ROOT.kMagenta, ROOT.kMagenta+2, ROOT.kMagenta-9, ROOT.kRed+1,ROOT.kAzure+7, ROOT.kBlue-7]
    allEta=sorted(set([key[1] for key in histos.keys()]))
    allEt=sorted(set([key[0] for key in histos.keys()]))
    for d in ['ee','fh','sci']:
        for h in ['cglob','hen']:
            for eta in allEta:

                quantH=ROOT.TH2F('%s_%s_eta%3.1f'%(h,d,eta),';log(E_{T}/[GeV]);Quantile;Value',len(allEt),0,len(allEt),nq,0,nq)

                toPlot=[]
                for ix in xrange(len(allEt)):
                    et=allEt[ix]
                    key=(et,eta)
                    if not key in histos : continue

                    #min. stat requirement
                    totalEvts=histos[key][d][h].Integral(0,histos[key][d][h].GetNbinsX()+1)
                    if totalEvts<100 : continue

                    toPlot.append( histos[key][d][h] )
                    toPlot[-1].Scale(1./totalEvts)
                    cidx=(len(toPlot)-1)%len(colours)
                    toPlot[-1].SetLineColor(colours[cidx])
                    lidx=(len(toPlot)-1)/len(colours)
                    toPlot[-1].SetLineStyle(1+lidx)
                    toPlot[-1].SetLineWidth(2)

                    #compute quantiles
                    toPlot[-1].GetQuantiles(nq,yq,xq)
                    quantH.GetXaxis().SetBinLabel(ix+1,'%3.0f'%et)
                    for iy in xrange(nq):
                        quantH.SetBinContent(ix+1,iy+1,yq[iy])

                if len(toPlot)==0 : continue
                showDistributions(toPlot,output=opt.output,name='%s%s_%s_eta%3.1f_%s'%(opt.tag,h,d,eta,wgtScheme),title='|#eta|=%3.1f'%eta)
                quantH.SetDirectory(outF)
                quantH.Write()

    #all done
    outF.Close()

if __name__ == "__main__":
    sys.exit(main())
