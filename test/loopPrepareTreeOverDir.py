import os,sys
import uuid
import optparse

def RunPacked(args):
    exe,cfg=args
    os.system('./bin/%s --cfg %s'%(exe,cfg))
    return True


def main():
    """wrapper to be used from command line"""
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('-e', '--exe',       dest='exe',         help='executable [%d]',           default='prepareCalibrationTree')
    parser.add_option('-s', '--sim',       dest='simTag',      help='simulation tag [%d]',       default='HGcal__version63_model2_BOFF')
    parser.add_option(      '--submit',    dest='subToCondor', help='submit to condor [%d]',     default=False, action='store_true')
    parser.add_option(      '--jobs',      dest='jobs',        help='parallel jobs [%d]',        default=1, type=int)
    parser.add_option('-r', '--reco',      dest='recoTag',     help='reconstruction tag [%d]',   default='DigiIC3_200u_version63_model2_BOFF')
    parser.add_option('-n', '--ninputs',   dest='pNinputs',    help='files per job [%d]',        default=10, type=int)
    parser.add_option('-o', '--out',       dest='outFile',     help='outFile name [%d]',         default='results/trees.root')
    (opt, args) = parser.parse_args()
    
    simFiles=[ f for f in os.listdir(args[0]) if opt.simTag in f ]
    nfiles=len(simFiles)
#    nfiles=100
    nchunks=nfiles/opt.pNinputs
    if nchunks*opt.pNinputs<nfiles: nchunks+=1
    print 'Found %d simulated files to loop over, dividing in %d chunks of %d'%(nfiles,nchunks,opt.pNinputs)

    FARMDIR='FARM_%s'%uuid.uuid1().hex
    os.system('mkdir -p %s'%FARMDIR)
    if opt.subToCondor:
        with open('%s/worker.sh'%FARMDIR,'w') as f:
            f.write('#!/bin/bash\n')            
            f.write('#environment setup\n')
            for key in os.environ:
                f.write('export %s="%s"\n'%(key,os.environ[key]))
            #try:
            #    cmssw=os.environ['CMSSW_BASE']            
            #    f.write('cd %s/src\n'%cmssw)
            #    f.write('eval `scram r -sh`\n')
            #    f.write('cd -')
            #except:
            #    pathname = os.path.abspath( os.path.dirname(sys.argv[0]) )
            #    f.write('cd %s/../../PFCalEE\n'%pathname)
            #    with open('%s/../../PFCalEE/g4env.sh'%pathname,'r') as g4env:                    
            #        for l in g4env.readlines(): 
            #            if '#!/bin/bash' in l: continue
            #            f.write('%s'%l)
            #    f.write('cd %s/../\n'%pathname)
            #    f.write('export PYTHONDIR=/afs/cern.ch/sw/lcg/external/Python/2.7.3/$ARCH/\n')
            #    f.write('export PYTHONPATH=$PYTHONPATH:$ROOTSYS/lib\n')
            #    f.write('export LD_LIBRARY_PATH=$ROOTSYS/lib:$PYTHONDIR/lib:`pwd`/lib:$LD_LIBRARY_PATH\n')
            f.write('#run the analysis\n')
            f.write('./bin/%s --cfg $0\n'%opt.exe)

    task_list=[]
    for i in xrange(0,nchunks):
        print '--- Start chunk #',i
        pStartInputs=i*opt.pNinputs
        cfgName='%s/cfg_%d.cfg'%(FARMDIR,i)
        with open(cfgName,'w') as cfg:
            cfg.write('pNevts=0\n')
            cfg.write('pStartInputs=%d\n'%pStartInputs)
            cfg.write('pNinputs=%d\n'%opt.pNinputs)
            cfg.write('inDir=%s\n'%args[0])
            cfg.write('recoTag=%s\n'%opt.recoTag)
            cfg.write('recoTreeName=RecoTree\n')
            cfg.write('simTag=%s\n'%opt.simTag)
            cfg.write('simTreeName=HGCSSTree\n')
            cfg.write('outFile=%s\n'%(opt.outFile.replace('.root','_%d.root'%i)))
            cfg.write('debug=0\n')
            cfg.write('enmin=1.0\n')
            if opt.exe=='prepareCalibrationTree':                
                cfg.write('dRwindow=0.5\n')
        task_list.append( (opt.exe,cfgName) )



    if opt.jobs<=1:
        for args in task_list: 
            RunPacked(args)
    else:
        from multiprocessing import Pool
        pool = Pool(opt.jobs)
        pool.map(RunPacked, task_list)


if __name__ == "__main__":
    sys.exit(main())
