import ROOT
import pickle

with open('calib_v63/pi-/cglobcalibPIOE_SciABS_SiABS_Sci_ABS.pck','r') as cache:
    pMap=pickle.load(cache)

c=ROOT.TCanvas('c','c',500,500)
c.SetTopMargin(0.06)
c.SetLeftMargin(0.15)
c.SetBottomMargin(0.1)
c.SetRightMargin(0.04) 

pname=['a','b','c','x_{0}']
for i in xrange(0,4):
    grEn=ROOT.TGraphErrors()
    grEn.SetMarkerStyle(20)
    for key in pMap:
        en=key[0]*ROOT.TMath.CosH(key[1])
        f=pMap[key].GetFunction('cfx')
        p,perr=f.GetParameter(i),f.GetParError(i)
        if perr<1e-3: continue
        np=grEn.GetN()
        grEn.SetPoint(np,en,p)
        grEn.SetPointError(np,0,perr)
    grEn.Sort()
    grEn.Draw('ap')
    grEn.GetXaxis().SetTitle('Energy [GeV]')
    grEn.GetYaxis().SetTitle(pname[i])
    p=ROOT.TF1('p','[0]*pow(log(x),2)+[1]*log(x)+[2]',1,1e4)
    grEn.Fit(p,'W+')
    c.SetLogx()
    c.Modified()
    c.Update()
    raw_input()

