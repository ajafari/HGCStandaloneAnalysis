import ROOT
import os
import sys
import numpy as np
from array import array
from myProgressBar import *
from layerWeightingSchemes import configureLayerWeights
from swCompensationTools import getCglob
from prepareRecHitProfiles import inputConfig
from calibrationPoints import *

def getQuantileMapping(qurl):
    """get quantile mappings per eta from ROOT file"""
    
    if not qurl : return None
    if not os.path.isfile(qurl) : return None

    inF=ROOT.TFile.Open(qurl)
    qmapping={}
    for key in inF.GetListOfKeys():
        kname=key.GetName()
        if not 'hen_' in kname: continue
        kname=kname.replace('hen_','')
        kname=kname.replace('eta','')
        d,genEta=kname.split('_')
        _,genEta=matchToSimPoint(1,float(genEta),getValue=True)
        h=key.ReadObj()
        for ix in xrange(1,h.GetNbinsX()+1):
            genEt=h.GetXaxis().GetBinLabel(ix)
            genEt=genEt.replace(' ','')
            if len(genEt)==0 : continue
            genEt,_=matchToSimPoint(float(genEt),genEta,getValue=True)
            qmapping[(d,genEt,genEta)]=[h.GetBinContent(ix,iy) for iy in xrange(1,h.GetNbinsY())]
    inF.Close()
    print qmapping
    return qmapping

def RunPacked(args):

    url,qurl,maxEvts,layerWeights,outdir=args
    print "In RunPacked, the url is %s" %(url)

    #get quantile mapping
    qmapping=getQuantileMapping(qurl)
    nq=10

    #fill tree
    hits=ROOT.TChain('hits')
    hits.AddFile(url)
    nentries=hits.GetEntries()
    if maxEvts>0 : nentries=min(nentries,maxEvts)
    print 'Hits chain @%s has %d events'%(url,nentries)

    #prepare the output
    os.system('mkdir -p {0}/swcomp'.format(os.path.dirname(outdir)))
    outUrl='{0}/swcomp/{1}.root'.format(os.path.dirname(outdir),os.path.splitext(os.path.basename(url))[0])   
    print "In RunPacked the out url is %s" %(outUrl)
    f = ROOT.TFile( outUrl, 'recreate' )
    t = ROOT.TTree( 'swcomp', 'software compensation')
    t.SetDirectory(f)

    cglob_ee = array( 'f', 10*[ 0. ] )
    t.Branch( 'cglob_ee',  cglob_ee,  'cglob_ee[10]/F' )
    cglob_fh = array( 'f', 10*[ 0. ] )
    t.Branch( 'cglob_fh',  cglob_fh,  'cglob_fh[10]/F' )
    cglob_si = array( 'f', 10*[ 0. ] )
    t.Branch( 'cglob_si',  cglob_si,  'cglob_si[10]/F' )
    cglob_sci = array( 'f',10*[ 0. ] )
    t.Branch( 'cglob_sci', cglob_sci, 'cglob_sci[10]/F' )

    en_q_ee = array( 'f', nq*[ 0. ] )
    t.Branch( 'en_q_ee', en_q_ee, 'en_q_ee[%d]/F'%nq )
    en_q_fh = array( 'f', nq*[ 0. ] )
    t.Branch( 'en_q_fh', en_q_fh, 'en_q_fh[%d]/F'%nq )
    en_q_sci = array( 'f', nq*[ 0. ] )
    t.Branch( 'en_q_sci', en_q_sci, 'en_q_sci[%d]/F'%nq )

    #loop over events
    for i in xrange(0,nentries):
        hits.GetEntry(i)
        key=matchToSimPoint(hits.genEt,hits.genEta,getValue=True)

        if i%50==0 : drawProgressBar(float(i)/float(nentries))

        #sort hit types
        eeHits=[]
        fhHits=[]
        sciHits=[]
        totalWgts=0
        for ih in xrange(0,hits.nhits):
            hen=hits.hit_en[ih]
            if layerWeights:
                lwgt=layerWeights.GetBinContent(hits.hit_lay[ih])
                hen *= lwgt
                totalWgts += lwgt
            if hits.hit_si[ih]:
                if hits.hit_lay[ih]<28:
                    eeHits.append(hen)
                else:
                    fhHits.append(hen)
            else:
                sciHits.append(hen)

        #global compensation factors
        for i in xrange(0,10):
            elim=2+i*0.5
            cglob_ee[i]  = getCglob(hcoll=np.array(eeHits),elim=elim)
            cglob_fh[i]  = getCglob(hcoll=np.array(fhHits),elim=elim)
            cglob_si[i]  = getCglob(hcoll=np.array(eeHits+fhHits),elim=elim)
            cglob_sci[i] = getCglob(hcoll=np.array(sciHits),elim=elim)

        #compute total energy per quantile
        for subDet,hitColl,enSum in [('ee',eeHits,en_q_ee),
                                     ('fh',fhHits,en_q_fh),
                                     ('sci',sciHits,en_q_sci)]:
            for iq in xrange(0,len(enSum)): enSum[iq]=0
            if not qmapping: continue
            qmap  = qmapping[(subDet, key[0],key[1])]
            for x in hitColl:
                lx=ROOT.TMath.Log(x)
                q=-1
                if lx<=qmap[0]:
                    q=0
                else:
                    for iq in xrange(1,len(qmap)):
                        if lx>qmap[iq]: continue
                        if lx<=qmap[iq-1]: continue
                        q=iq
                        break
                enSum[q]+=x

        #all done for this event
        f.cd()
        t.Fill()

    #save file
    f.cd()
    t.Write()
    f.Close()
    print 'Friend tree with software compensation variables @',outUrl

def main():
    """wrapper to be used from command line"""

    (opt, args) = inputConfig(argList=[(u'-q',u'--qfile'),(u'--jobs',)],
                              kwargList=[{'dest':'qfile','help':'quantile definitions [%default]', 'default':None},
                                         {'dest':'njobs','help':'number of jobs to run in parallel [%default]', 'default':8, 'type':int}])
    print 'Output will be written in %s' %opt.output 
    #get files to analyze
    urlList=[os.path.join(opt.input,x) for x in os.listdir(opt.input) if opt.tag in x]
    print 'Found %d files in %s to analyze, matching %s'%(len(urlList),opt.input,opt.tag)
    #weigthing scheme
    layerWeights,wgtScheme=configureLayerWeights(opt,urlList[0])


    #run tree creation in parallel
    task_list=[ (x,opt.qfile,opt.maxEvts,layerWeights,opt.output) for x in urlList ]
    from multiprocessing import Pool
    pool = Pool(opt.njobs)
    pool.map(RunPacked, task_list)


if __name__ == "__main__":
    sys.exit(main())
