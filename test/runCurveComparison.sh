#global compensation

files="std:calib_v63/gamma/resol_dedx_avgABS.root"
files="${files},gc:calib_v63/gamma/resol_dedx_avgABSGC.root"
python test/compareCurves.py  -i ${files} -o ~/www/HGCal/Standalone/4Jun/v63_gc_gamma --yran 0.8,1.1 --logx --ytitle 'Ratio to std'

files="std:calib_v63/pi-/resol_dedx_avgRESPIOE_SciABS_SiPIOE_SiABS_Sci.root"
files="${files},gc:calib_v63/pi-/resol_dedx_avgABS_SciRESPIOE_SiGCABS_SiPIOE_Sci.root"
python test/compareCurves.py  -i ${files} -o ~/www/HGCal/Standalone/4Jun/v63_gc_pi --yran 0.4,1.2 --logx --ytitle 'Ratio to std'

files="TDR:calib_v63/pi-/resol_dedx_avgABS_SciRESPIOE_SiGCABS_SiPIOE_Sci.root"
files="${files},12+12:calib_v64/pi-/resol_dedx_avgABS_SciRESPIOE_SiGCABS_SiPIOE_Sci.root"
files="${files},18:calib_v65/pi-/resol_dedx_avgABS_SciRESPIOE_SiGCABS_SiPIOE_Sci.root"
files="${files},9+9:calib_v66/pi-/resol_dedx_avgABS_SciRESPIOE_SiGCABS_SiPIOE_Sci.root"
python test/compareCurves.py  -i ${files} -o ~/www/HGCal/Standalone/4Jun/geom_gcresol_pi --yran 0.5,2.5 --logx --ytitle 'Ratio to TDR'

#PION resolution
files="TDR:calib_v63/pi-/resol_dedx_avgRESPIOE_SciABS_SiPIOE_SiABS_Sci.root"
#files="${files},15+15:calib_v67/pi-/resol_dedx_avgRESPIOE_SciABS_SiPIOE_SiABS_Sci.root"
files="${files},12+12:calib_v64/pi-/resol_dedx_avgRESPIOE_SciABS_SiPIOE_SiABS_Sci.root"
files="${files},18:calib_v65/pi-/resol_dedx_avgRESPIOE_SciABS_SiPIOE_SiABS_Sci.root"
files="${files},9+9:calib_v66/pi-/resol_dedx_avgRESPIOE_SciABS_SiPIOE_SiABS_Sci.root"
python test/compareCurves.py  -i ${files} -o ~/www/HGCal/Standalone/4Jun/geom_cbresol_pi  --yran 0.5,2.5 --logx --ytitle 'Ratio to TDR'

files="TDR:calib_v630/pi-/resol_dedx_avgPIOE_SciABS_Sci.root"
#files="${files},15+15:calib_v670/pi-/resol_dedx_avgPIOE_SciABS_Sci.root"
files="${files},12+12:calib_v640/pi-/resol_dedx_avgPIOE_SciABS_Sci.root"
files="${files},18:calib_v650/pi-/resol_dedx_avgPIOE_SciABS_Sci.root"
files="${files},9+9:calib_v660/pi-/resol_dedx_avgPIOE_SciABS_Sci.root"
python test/compareCurves.py  -i ${files} -o ~/www/HGCal/Standalone/4Jun/geom_resol_pi_scionly --yran 0.5,5 --logx --ytitle 'Ratio to TDR'

#files="3%:calib_v63/pi-/resol_dedx_avgRESPIOE_SciABS_SiPIOE_SiABS_Sci.root"
##files="${files},5%:calib_v63_5/pi-/resol_dedx_avgRESPIOE_SciABS_SiPIOE_SiABS_Sci.root"
##files="${files},10%:calib_v63_10/pi-/resol_dedx_avgRESPIOE_SciABS_SiPIOE_SiABS_Sci.root"
#files="${files},15%:calib_v63_15/pi-/resol_dedx_avgRESPIOE_SciABS_SiPIOE_SiABS_Sci.root"
#python test/compareCurves.py  -i ${files} -o ~/www/HGCal/Standalone/4Jun/ic_resol_pi --yran 0.9,1.1

#files="3%:calib_v63/pi-/response_dedx_avgRESPIOE_SciABS_SiPIOE_SiABS_Sci.root"
##files="${files},5%:calib_v63_5/pi-/response_dedx_avgRESPIOE_SciABS_SiPIOE_SiABS_Sci.root"
##files="${files},10%:calib_v63_10/pi-/response_dedx_avgRESPIOE_SciABS_SiPIOE_SiABS_Sci.root"
#files="${files},15%:calib_v63_15/pi-/response_dedx_avgRESPIOE_SciABS_SiPIOE_SiABS_Sci.root"
#python test/compareCurves.py  -i ${files} -o ~/www/HGCal/Standalone/4Jun/ic_response_pi --yran 0.98,1.08
 
#GAMMA resolution
files="TDR:calib_v63/gamma/resol_dedx_avgABS.root"
#files="${files},15+15:calib_v67/gamma/resol_dedx_avgABS.root"
files="${files},12+12:calib_v64/gamma/resol_dedx_avgABS.root"
files="${files},18:calib_v65/gamma/resol_dedx_avgABS.root"
files="${files},9+9:calib_v66/gamma/resol_dedx_avgABS.root"
python test/compareCurves.py  -i ${files} -o ~/www/HGCal/Standalone/4Jun/geom_resol_gamma --yran 0.7,1.3 --logx --ytitle 'Ratio to TDR'


files="TDR:calib_v630/gamma/resol_dedx_avgABS.root"
#files="${files},15+15:calib_v640/gamma/resol_dedx_avgABS.root"
files="${files},12+12:calib_v640/gamma/resol_dedx_avgABS.root"
files="${files},18:calib_v650/gamma/resol_dedx_avgABS.root"
files="${files},9+9:calib_v660/gamma/resol_dedx_avgABS.root"
python test/compareCurves.py  -i ${files} -o ~/www/HGCal/Standalone/4Jun/geom_resol_gamma_scionly --yran 0.5,5.0 --logx --ytitle 'Ratio to TDR'


# GAMMA RESPONSE
files="TDR:calib_v63/gamma/response_dedx_avg.root"
#files="${files},15+15:calib_v67/gamma/response_dedx_avg.root"
files="${files},12+12:calib_v64/gamma/response_dedx_avg.root"
files="${files},18:calib_v65/gamma/response_dedx_avg.root"
files="${files},9+9:calib_v66/gamma/response_dedx_avg.root"
python test/compareCurves.py  -i ${files} -o ~/www/HGCal/Standalone/4Jun/geom_response_gamma --yran 0.8,1.4 --logx --ytitle 'Ratio to TDR'

files="TDR:calib_v630/gamma/response_dedx_avgABS.root"
#files="${files},15+15:calib_v670/gamma/response_dedx_avgABS.root"
files="${files},12+12:calib_v640/gamma/response_dedx_avgABS.root"
files="${files},18:calib_v650/gamma/response_dedx_avgABS.root"
files="${files},9+9:calib_v660/gamma/response_dedx_avgABS.root"
python test/compareCurves.py  -i ${files} -o ~/www/HGCal/Standalone/4Jun/geom_response_gamma_scionly --yran 0.8,1.4 --logx --ytitle 'Ratio to TDR'


#pioe
files="TDR:calib_v63/pi-/pioe_dedx_avgPIOE_SciABS_SiABS_Sci.root"
#files="${files},15+15:calib_v67/pi-/pioe_dedx_avgPIOE_SciABS_SiABS_Sci.root"
files="${files},12+12:calib_v64/pi-/pioe_dedx_avgPIOE_SciABS_SiABS_Sci.root"
files="${files},18:calib_v65/pi-/pioe_dedx_avgPIOE_SciABS_SiABS_Sci.root"
files="${files},9+9:calib_v66/pi-/pioe_dedx_avgPIOE_SciABS_SiABS_Sci.root"
python test/compareCurves.py  -i ${files} -o ~/www/HGCal/Standalone/4Jun/geom_pioe --yran 0.8,1.4 --logx --noratio  --ytitle '#gamma / #pi'

files="TDR:calib_v630/pi-/pioe_dedx_avgABS_Sci.root"
#files="${files},15+15:calib_v670/pi-/pioe_dedx_avgABS_Sci.root"
files="${files},12+12:calib_v640/pi-/pioe_dedx_avgABS_Sci.root"
files="${files},18:calib_v650/pi-/pioe_dedx_avgABS_Sci.root"
files="${files},9+9:calib_v660/pi-/pioe_dedx_avgABS_Sci.root"
python test/compareCurves.py  -i ${files} -o ~/www/HGCal/Standalone/4Jun/geom_pioe_scionly --yran 0.5,5.0 --logx --noratio --ytitle '#gamma / #pi'
