#!/usr/bin/env python

import os
import sys
import ROOT

baseDir=sys.argv[1]

for f in os.listdir(baseDir):
    fIn=ROOT.TFile.Open(os.path.join(baseDir,f))

    toDel=False
    try:
        if fIn.IsZombie() or fIn.TestBit(ROOT.TFile.kRecovered): toDel=True
        fIn.Close()
    except:
        toDel=True


    if not toDel: continue
    print 'Removing',f
    os.system('rm -v %s'%os.path.join(baseDir,f))


