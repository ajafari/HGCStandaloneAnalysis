import ROOT

def getAveragedWeights(h):
    """averages the weights to apply per layer from neighboring layers"""
    avgH=h.Clone('{0}_avg'.format(h.GetName()))
    avgH.SetDirectory(0)
    for xbin in xrange(1,h.GetNbinsX()+1):
        avgWgt=0.5*(h.GetBinContent(xbin)+h.GetBinContent(xbin+1))
        h.SetBinContent(xbin,avgWgt)
    return avgH

def getWeights(url=None,baseWeightingSchemes=['dedx','x0','lambda'],strictWeightsOnly=False,descopeMode=-1,isCMSSW=False):
    """ builds the weights to apply in each layer for calibration """

    layerWeights={}

    #standalone weights are in histograms
    if not isCMSSW:
        print 'Standalone mode: retrieving weights for',baseWeightingSchemes,'from ROOT file'

        fIn=ROOT.TFile.Open(url)

        weightingSchemes=[w for w in baseWeightingSchemes]
        if not strictWeightsOnly: 
            weightingSchemes+= ['%s_avg'%w for w in baseWeightingSchemes if len(w)>0]
            weightingSchemes+= ['%s_descoped'%w for w in baseWeightingSchemes if len(w)>0]

        #read the layer weights stored in file
        for key in baseWeightingSchemes:
            layerWeights[key]=fIn.Get(key)
            layerWeights[key].SetDirectory(0)
        
            #if file has been hadded the weights are multiplied by the number of original files
            nbins=float(layerWeights[key].GetNbinsX())
            nentries=layerWeights[key].GetEntries()
            sf=nbins/nentries  
            layerWeights[key].Scale(sf)
            print 'Scaling',key,',weights by',sf

            #de-scoping weights (ignore specific layers)
            if descopeMode>=0:
                desKey=key+'_descoped'
                layerWeights[desKey]=layerWeights[avgKey].Clone('{0}_descoped'.format(key))
                layerWeights[desKey].SetDirectory(0)
                dropLayers=1
                keepLayers=[29,31,33,35,37,39]
                if descopeMode==1:
                    dropLayers=2
                    keepLayers=[29,32,35,38]
                if descopeMode==2:
                    dropLayers=3
                    keepLayers=[29,33,37]
                for xbin in keepLayers:
                    totalEn=sum([layerWeights[key].GetBinContent(ibin) for ibin in xrange(xbin,xbin+dropLayers+1) ])
                    layerWeights[desKey].SetBinContent(xbin,totalEn)
                for ibin in xrange(xbin+1,xbin+dropLayers+1): layerWeights[desKey].SetBinContent(ibin,0.)
                for xbin in keepLayers:
                    avgWgt=(1./(dropLayers+1.0))*sum([layerWeights[desKey].GetBinContent(ibin) for ibin in xrange(xbin,xbin+dropLayers+1)])
                    layerWeights[desKey].SetBinContent(xbin,avgWgt)
                print 'Descoped average layer weights generated for',key,'in mode',descopeMode
            
            #average out from neighboring weights                
            if not strictWeightsOnly: layerWeights[key+'_avg']=getAveragedWeights(layerWeights[key])

        #all done with the file
        fIn.Close()

    else:
        print 'CMSSW mode: using weights from RecHitCalibration'

        from RecHitCalibration import RecHitCalibration
        calib=RecHitCalibration()
        layerWeights['dedx']=ROOT.TH1F('dedx','dedx',len(calib.dEdX_weights),0,len(calib.dEdX_weights))
        layerWeights['dedx'].SetDirectory(0)
        for i in xrange(1,len(calib.dEdX_weights)):
            layerWeights['dedx'].SetBinContent(i,calib.dEdX_weights[i])
            layerWeights['dedx'].SetBinError(i,0)
        if not strictWeightsOnly: layerWeights['dedx_avg']=getAveragedWeights(layerWeights['dedx'])
        
    #trivial weights for cross check
    layerWeights['trivial']=layerWeights[ layerWeights.keys()[0] ].Clone('trivial')
    for i in xrange(0,layerWeights['trivial'].GetNbinsX()): layerWeights['trivial'].SetBinContent(i+1,1.0)

    #print out weights
    print '======= Weight modes ========'
    print 'Lay',
    for key in layerWeights: print key,
    print ''
    for i in xrange(0,layerWeights['trivial'].GetNbinsX()):
        print '%2d'%(i+1),
        for key in layerWeights:
            print '%3.3f'%layerWeights[key].GetBinContent(i+1),
        print ''
    print '============================='

    return layerWeights

def configureLayerWeights(opt,url):
    """configure the layer weights to be used based on parsed options"""
    wgtScheme=opt.wgt
    layerWeights=None
    if opt.wgt!='trivial':
        layerWeights=getWeights(url=url,
                                baseWeightingSchemes=[opt.wgt],
                                strictWeightsOnly=opt.strictWgts,
                                descopeMode=opt.descopeMode,
                                isCMSSW=opt.cmssw)
        wgtScheme=layerWeights.keys()[0]
        layerWeights=layerWeights[wgtScheme]
    return layerWeights,wgtScheme
