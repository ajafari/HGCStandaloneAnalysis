import ROOT
import os
import pickle
from rootTools import showGraphCollection
from calibrationPoints import *

def doPiOverETypeCalibration(opt,fitResults,tag,outDir,elimIdx):
    print 'Not yet implemented'

    pioeGr={}
    pioeRecGr={}
    for v in fitResults:

        pioeGr[v]={}
        pioeRecGr[v]={}

        #fill response and relative resolution
        maxEn=0
        for iet,ieta in fitResults[v]:

            et,eta=ET_PTS[iet],ETA_PTS[ieta]
            en=et*ROOT.TMath.CosH(eta)          
            maxEn=max(en,maxEn)

            if not ieta in pioeGr[v]:
                baseName='%d_%s_%s'%(ieta,v,elimIdx)
                pioeGr[v][ieta]=ROOT.TGraphErrors()
                pioeGr[v][ieta].SetMarkerStyle(20+ieta)
                pioeGr[v][ieta].SetName('pioe_%s'%baseName)
                pioeGr[v][ieta].SetTitle('#eta=%3.2f'%eta)
                pioeRecGr[v][ieta]=pioeGr[v][ieta].Clone('recpioe_%s'%baseName)

            meanFit,meanFit_error,sigmaFit,sigmaFit_error,corr=fitResults[v][(iet,ieta)]

            np=pioeGr[v][ieta].GetN()
            pioeGr[v][ieta].SetPoint(np,en,en/meanFit)
            pioeGr[v][ieta].SetPointError(np,0,en*(meanFit_error/meanFit**2))
            pioeRecGr[v][ieta].SetPoint(np,meanFit,en/meanFit)
            pioeRecGr[v][ieta].SetPointError(np,meanFit_error,en*(meanFit_error/meanFit**2))

        toDelete=[]
        for eta in pioeGr[v]:
            pioeGr[v][ieta].Sort()
            pioeRecGr[v][ieta].Sort()
            if pioeGr[v][ieta].GetN()<2: toDelete.append(eta)
            
        #remove entries with less than 3 points
        for eta in toDelete:
            del pioeGr[v][ieta]
            del pioeRecGr[v][ieta]
    
    #save calibration file
    pioeGr2D={}
    pioeRecGr2D={}
    for v in pioeGr:
        pioeGr2D[v]=ROOT.TGraph2D()
        pioeGr2D[v].SetName('pioe_%s_%s'%(v,elimIdx))
        pioeRecGr2D[v]=pioeGr2D[v].Clone('recpioe_%s_%s'%(v,elimIdx))
        x,y=ROOT.Double(0),ROOT.Double(0)
        for eta in pioeGr[v]:
            npts=pioeGr[v][ieta].GetN()
            if npts<2 : continue
            for np in xrange(0,npts):
                pioeGr[v][ieta].GetPoint(np,x,y)
                pioeRecGr[v][ieta].GetPoint(np,x,y)
                ip=pioeGr2D[v].GetN()
                pioeGr2D[v].SetPoint(ip,float(x),eta,float(y))
                pioeRecGr2D[v].SetPoint(ip,float(x),eta,float(y))

    calibFile='calibresults%s_PIOE_%s.pck'%(tag,elimIdx)
    pck=os.path.join(outDir,calibFile)
    with open(pck, 'w') as cachefile:
        pickle.dump(pioeRecGr2D, cachefile, pickle.HIGHEST_PROTOCOL)
        pickle.dump(pioeRecGr, cachefile, pickle.HIGHEST_PROTOCOL)
        pickle.dump(pioeGr2D, cachefile, pickle.HIGHEST_PROTOCOL)
        pickle.dump(pioeGr, cachefile, pickle.HIGHEST_PROTOCOL)
    print 'pi/e calibration stored in',calibFile

    #show graph
    for v in pioeGr:
        for grColl,name,xtitle,ytitle,func in [
            (pioeGr[v],    os.path.join(outDir,'pioe_%s%s%s.png'%(v,tag,elimIdx)),    'E [GeV]',             '#gamma / #pi',None),
            (pioeRecGr[v], os.path.join(outDir,'recpioe_%s%s%s.png'%(v,tag,elimIdx)), '<E_{rec}^{em}> [GeV]','#gamma / #pi',None)
            ] : 
            showGraphCollection(grColl,name,xtitle,ytitle,func,logx=True)

