#!/bin/bash

BOLD='\033[1m'
RED='\033[0;31m'
BLUE='\033[0;36m'
NC='\033[0m' 

hgc=${1}
bh=${2}

gWorkspace=Geant4_gamma_0_workspace.root
piWorkspace=Geant4_pi-_0_workspace.root
#baseDir=/eos/cms/store/cmst3/user/psilva/HGCal/FHGranularity
baseDir=/eos/cms/store/user/ajafari/HGCAL/
commonOpts="-b --minEta 1.5 --maxEta 2.3 --minE 0";

echo "----------------------------------------------------------------------"
echo -e "${BOLD}Energy scale calibration sequence for v${hgc} with sci-alone v${bh})${NC}"
echo "----------------------------------------------------------------------"
echo -e "${BLUE}"
echo "Workspaces are"
echo "   gamma: ${gWorkspace}"
echo "   pi-:   ${piWorkspace}"
echo "Base directory is ${baseDir}"
echo -e "${NC}"

echo -e "${BOLD}Electromagnetic scale${NC}"
for elim in 0 1 2 3 4 5 6 7 8 9; do    
    for v in ${hgc} ${bh}; do
    	echo -e "${RED}Calibrating with single photons v${v} ${NC}"
    	cglob=cglob_si
    	echo ${cglob}
    	if [ "${v}" = "${bh}" ]; then
            cglob=cglob_sci
    	fi
    	echo "elim is ${elim}"
    	io="-i ${baseDir}/version${v}/workspace/${gWorkspace} --out calib_v${v}/gamma"
    	specOpts="-c ABS --cglob ${cglob} --elimId ${elim}";
        python test/runCalibrationFits.py ${commonOpts} ${specOpts} ${io};
    	calibStr="ABS:calib_v${v}/gamma/calibresults_ABS_${elim}.pck"
    	python test/runCalibrationFits.py ${commonOpts} ${specOpts} ${io} --applyCalib ${calibStr};
    	if [ "${v}" = "${hgc}" ]; then 
            calibStr="${calibStr},GC:calib_v${v}/gamma/cglobcalibABS_ABS_${elim}.pck"
            python test/runCalibrationFits.py ${commonOpts} ${specOpts} ${io} --applyCalib ${calibStr};
    	fi
    done
    echo -e "${BOLD}Pion energy scale ${NC}"
    echo -e "${RED}Scintillator standalone v${bh}${NC}"
    io="-i ${baseDir}/version${bh}/workspace/${piWorkspace} --out calib_v${bh}/pi-"
    specOpts="--cglob cglob_sci --noiseResol --elimId ${elim}"
    calibStr="ABS_Sci:calib_v${bh}/gamma/calibresults_ABS_${elim}.pck"
    python test/runCalibrationFits.py ${commonOpts} ${specOpts} ${io} -c PIOE --applyCalib ${calibStr}
    calibStr="${calibStr},PIOE_Sci:calib_v${bh}/pi-/calibresultsABS_Sci_PIOE_${elim}.pck"
    python test/runCalibrationFits.py ${commonOpts} ${specOpts} ${io} -c ABS  --applyCalib ${calibStr}
    
    echo -e "${RED}Full HGC v${hgc}${NC}"
    io="-i ${baseDir}/version${hgc}/workspace/${piWorkspace} --out calib_v${hgc}/pi-"
    specOpts="--cglob cglob_fh --noiseResol --elimId ${elim}"
    calibStr="ABS_Si:calib_v${hgc}/gamma/calibresults_ABS_${elim}.pck"
    calibStr="${calibStr},ABS_Sci:calib_v${bh}/gamma/calibresults_ABS_${elim}.pck"
    calibStr="${calibStr},PIOE_Sci:calib_v${bh}/pi-/calibresultsABS_Sci_PIOE_${elim}.pck"
    python test/runCalibrationFits.py ${commonOpts} ${specOpts} ${io} -c PIOE --applyCalib ${calibStr}
    calibStr="${calibStr},PIOE_Si:calib_v${hgc}/pi-/calibresultsPIOE_SciABS_SiABS_Sci_PIOE_${elim}.pck"
    python test/runCalibrationFits.py ${commonOpts} ${specOpts} ${io} -c ABS  --applyCalib ${calibStr}
    calibStr="${calibStr},RES:calib_v${hgc}/pi-/rescalibresultsPIOE_SciABS_SiPIOE_SiABS_Sci_ABS_${elim}.pck"
    python test/runCalibrationFits.py ${commonOpts} ${specOpts} ${io} -c ABS  --applyCalib ${calibStr}
    calibStr="${calibStr},GC:calib_v${hgc}/pi-/cglobcalibRESPIOE_SciABS_SiPIOE_SiABS_Sci_ABS_${elim}.pck"
    python test/runCalibrationFits.py ${commonOpts} ${specOpts} ${io} -c ABS  --applyCalib ${calibStr}
done


