#!/usr/bin/env python

import ROOT
import numpy as numpy
import json
import os
import optparse
from myProgressBar import *
from layerWeightingSchemes import getWeights
import sys
import pickle

def fillWorkspacePacked(args):
    """wrapper to run in parallel the conversion from CMSSW"""
    url,nlayers,tag,outDir=args

    from StandaloneWrapper import SACevent
    from NtupleDataFormat import HGCalNtuple
    ntuple=HGCalNtuple(url)
    nentries=ntuple.nevents()
    print 'Reading hits from %d events in %s'%(nentries,url)

    entry=0
    allMatchedHits=[]
    for event in ntuple:
        if entry%5==0 : drawProgressBar(float(entry)/float(nentries))
        sacev=SACevent(event,nlayers)
        allMatchedHits += sacev.matchedHitSums 
        entry+=1

    pckFileUrl=os.path.join(outDir,os.path.basename(url.replace('.root','{0}.pck'.format(tag if tag else ''))))
    with open(pckFileUrl,'w') as cache:
        pickle.dump(allMatchedHits,cache,pickle.HIGHEST_PROTOCOL)
    print 'Summaries saved @',pckFileUrl





def fillWorkspaceFrom(ws,obj,icpfix,layerWeights,enSF=1.0,gcidx=6):
    """fills workspace from a tuple"""
    
    #generator level information
    genEta=round(obj.genEta,1)
    genEt=round(obj.genEt,0) 
    genEn=genEt*ROOT.TMath.CosH(genEta)
    ws.var('en').setVal(genEn)
    ws.var('et').setVal(genEt)
    ws.var('eta').setVal(genEta)
    ws.var('phi').setVal(obj.genPhi)

    newEntry=ROOT.RooArgSet(ws.var('en'), ws.var('et'), ws.var('eta'),  ws.var('phi'))
        
    #energy estimators
    for key in layerWeights:
        nlay=layerWeights[key].GetNbinsX()
        totalRec={'si':0,'sci':0}
        si_sumen=getattr(obj,'si_sumen%s'%icpfix)
        sci_sumen=getattr(obj,'sci_sumen%s'%icpfix)
        for ilay in xrange(0,nlay):
            if ilay>=len(si_sumen) : continue
            wgt=layerWeights[key].GetBinContent(ilay+1)
            
            #silicon
            totalRec['si']+=wgt*si_sumen[ilay]               
        
            #scintillator
            totalRec['sci']+=wgt*obj.sci_sumen[ilay]

        for det in totalRec:
            ws.var('en_%s_%s'%(det,key)).setVal(totalRec[det])
            newEntry.add(ws.var('en_%s_%s'%(det,key)))

    #global compensation factors
    try:
        for gcIdx in xrange(0,10):
            ws.var('cglob_ee_%s' %(gcIdx)).setVal(min(max(obj.cglob_ee[gcIdx],-1.),2.))
            ws.var('cglob_fh_%s' %(gcIdx)).setVal(min(max(obj.cglob_fh[gcIdx],-1.),2.))
            ws.var('cglob_si_%s' %(gcIdx)).setVal(min(max(obj.cglob_si[gcIdx],-1.),2.))
            ws.var('cglob_sci_%s' %(gcIdx)).setVal(min(max(obj.cglob_sci[gcIdx],-1.),2.))
    except Exception as e:
        print e
        pass
    for gcIdx in xrange(0,10):
        newEntry.add( ws.var('cglob_ee_%s' %(gcIdx)))
        newEntry.add( ws.var('cglob_fh_%s' %(gcIdx)))
        newEntry.add( ws.var('cglob_si_%s' %(gcIdx)))
        newEntry.add( ws.var('cglob_sci_%s' %(gcIdx)))

    #add new entry
    ws.data('data').add( newEntry )

    #return Et,Eta
    return genEt,genEta



def prepareWorkspace(urlList,layerWeights,icpfix,outDir,friendPath,tag=None,isCMSSW=False,gcidx=6):
    """Computes energy sums and stores in workspace for calibraiton"""

    #prepare the workspace
    ws=ROOT.RooWorkspace("w")
    dsVars=ROOT.RooArgSet( ws.factory('en[0,0,9999999999]'), 
                           ws.factory('et[0,0,9999999999]'), 
                           ws.factory('eta[1.5,1.45,3.1]'), 
                           ws.factory('phi[0,-3.2,3.2]') )
    for det in ['si','sci']:
        for key in layerWeights:
            dsVars.add( ws.factory('en_%s_%s[0,0,99999999]'%(det,key)) )    
    for gcIdx in xrange(0,10):
        dsVars.add(ws.factory('cglob_ee_%s[1.0,0.,2.]' %(gcIdx)))
        dsVars.add(ws.factory('cglob_fh_%s[1.0,0.,2.]' %(gcIdx)))
        dsVars.add(ws.factory('cglob_si_%s[1.0,0.,2.]' %(gcIdx)))
        dsVars.add(ws.factory('cglob_sci_%s[1.0,0.,2.]' %(gcIdx)))

    getattr(ws,'import')( ROOT.RooDataSet('data','data',dsVars) )

    simPoints=set()
    if not isCMSSW:

        #chain all files
        hits=ROOT.TChain('hits')
        swcomp=ROOT.TChain('swcomp')
        for f in urlList: 
            hits.AddFile(f)
            #swcompf='%s/%s/%s'%(os.path.dirname(f),'swcomp',os.path.basename(f))
            swcompf='%s/%s'%(os.path.dirname(friendPath),os.path.basename(f))
            if os.path.isfile(swcompf) :
                swcomp.AddFile(swcompf)
        if swcomp.GetEntries()==hits.GetEntries():
            hits.AddFriend(swcomp)
            print 'Added SW compensation tree as friend, will use index',gcidx
        else:
            print 'Unable to add SW compensation tree as friend #events=',swcomp.GetEntries()
            print hits.GetEntries(),'events were expected'
            
        nentries=hits.GetEntries()
        print 'Reading hits from %d events'%nentries
        for entry in xrange(0,nentries+1):
            hits.GetEntry(entry)
            if entry%1000==0 : drawProgressBar(float(entry)/float(nentries))
            simPoints.add( fillWorkspaceFrom(ws,hits,icpfix,layerWeights,1.0e-3,gcidx) )

    else:
 
        #if ROOT file convert the raw ntuples to layer-summed summaries in pickle files
        nlayers=layerWeights[ layerWeights.keys()[0] ].GetNbinsX()
        tasklist=[(f,nlayers,tag,outDir) for f in files if '.root' in f and not '_workspace' in f]
        if len(tasklist)>0:
            import multiprocessing as MP
            pool = MP.Pool(8)
            pool.map(fillWorkspacePacked,tasklist)

        #if pickle files use them to fill the workspace
        for f in urlList:
            if not '.pck' in f: continue
            cache=open(f,'r')
            sacevList=pickle.load(cache)
            print '...adding',len(sacevList),'points'
            for hits in sacevList:
                simPoints.add( fillWorkspaceFrom(ws,hits,icpfix,layerWeights,1.0,gcidx) )    
            cache.close()
        
    #all done, write to file    
    #wsFileUrl=os.path.join(outDir,urlList[0].replace('.root','{0}_workspace{1}.root'.format(tag if tag else '',icpfix)))
    wsFileUrl=os.path.join(outDir,os.path.basename(urlList[0].replace('.root','{0}_workspace{1}.root'.format(tag if tag else '',icpfix))))
    ws.writeToFile(wsFileUrl,True)
    print '\nCreated the analysis RooDataSet with %d events, stored @ %s'%(ws.data('data').numEntries(),wsFileUrl)
    
    fOut=ROOT.TFile.Open(wsFileUrl,'UPDATE')
    simPointsGr=ROOT.TGraph()
    simPointsGr.SetName('simPoints')
    for pt in simPoints:
        np=simPointsGr.GetN()
        simPointsGr.SetPoint(np,pt[0],pt[1])
    simPointsGr.Sort()
    simPointsGr.Write()
    fOut.Close()


def main():
    """wrapper to be used from command line"""

    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('-i', '--in' ,          dest='input',       help='Input directory [%default]',              default=None)    
    parser.add_option('-f', '--friend' ,      dest='friend',       help='Friend directory with swcomp in [%default]',              default=None)    
    parser.add_option(      '--match',        dest='match',       help='file name to match [%default]',           default='Geant4_pi-_')    
    parser.add_option('-o', '--out' ,         dest='output',      help='output dir (by default the same as the input)', default=None)    
    parser.add_option('-t', '--tag' ,         dest='tag',         help='tag',                   default=None)    
    parser.add_option(      '--ic' ,          dest='icpfix',      help='branch with different i.c. conditions [%default]', default='')
    parser.add_option(      '--gc' ,          dest='gcidx',       help='index for global compensation [%default]', default=6, type=int)
    parser.add_option('-w', '--wgt' ,         dest='wgt',         help='weighting scheme [%default]',  default='dedx')    
    parser.add_option('-s', '--strictWgts' ,  dest='strictWgts',  help='use strict weights', action='store_true', default=False)
    parser.add_option(      '--descopeMode' , dest='descopeMode', help='descope mode', default=-1, type=int)
    parser.add_option(      '--cmssw' ,       dest='cmssw',       help='use cmssw inputs', default=False, action='store_true')
    (opt, args) = parser.parse_args()

    if not opt.output:
        print 'To avoid further crashes due to access authentication, PROVIDE output directory!!!!'
        exit()
        #opt.output=os.path.dirname(opt.input)
        #print 'Setting output directory to be the same as the input',opt.output
    else:
        os.system('mkdir -p %s'%opt.output)

    if opt.cmssw:
        sys.path.append('%s/src/ntuple-tools'%os.environ['CMSSW_BASE'])

    urlList=[os.path.join(opt.input,x) for x in os.listdir(opt.input) if opt.match in x and not '_workspace' in x]
    print 'Found %d files in %s to analyze, matching %s'%(len(urlList),opt.input,opt.match)  

    layerWeights=getWeights(url=urlList[0],                     
                            baseWeightingSchemes=opt.wgt.split(','),
                            strictWeightsOnly=opt.strictWgts,
                            descopeMode=opt.descopeMode,
                            isCMSSW=opt.cmssw)

    prepareWorkspace(urlList=urlList,                     
                     layerWeights=layerWeights,
                     icpfix=opt.icpfix,
                     outDir=opt.output,
                     friendPath=opt.friend,
                     tag=opt.tag,                     
                     isCMSSW=opt.cmssw,
                     gcidx=opt.gcidx)
    
    
if __name__ == "__main__":
    sys.exit(main())

