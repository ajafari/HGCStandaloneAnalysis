import numpy as np
import ROOT
from rootTools import buildTH2Profile
from calibrationPoints import *

def getCglob(hcoll,elim):
    """Compute correction factor for global software compensation following the description of arXiv:1207.4210"""
    mhen=np.mean(hcoll, dtype=np.float64)
    nabove=len(hcoll[hcoll<elim])
    nbelow=len(hcoll[hcoll<mhen])
    cglob=float(nabove)/float(nbelow) if nbelow>0 else -1
    return cglob

def parametrizeCglob(h):
    """parametrizes the global compensation factor versus resolution"""
    gr=buildTH2Profile(h)
    if gr.GetN()>5:
        f=ROOT.TF1('cfx','x<[3] ? [0]*x+[1] : ([0]+([1]-[2])/[3])*x+[2]',0,2)
        f.SetParName(0,'a')
        f.SetParLimits(0,-5.0,0.0)
        f.SetParName(1,'b')
        f.SetParLimits(1,0.0,2.0)
        f.SetParName(2,'c')
        f.SetParLimits(2,-2.0,0.0)
        f.SetParName(3,'x_{0}')
        f.SetParLimits(3,1.0,1.2)
        gr.Fit(f,'MRQ+','',0,2)
    return gr

def evalCglobCorrection(x,a,b,c,x0):
    """evaluates the parametrized cglob value and returns the energy scale factor to apply"""
    cglob=a*x+b if x<x0 else (a+(b-c)/x0)*x+c    
    return 1.0/(cglob+1.0)

def parametrizeCglobVersusEn(cglobParamMap):
    """fits the evolution of the cglob parameterization parameters as function of the energy"""

    cglobParamEvol=[]

    for i in xrange(0,4):
        cglobParamEvol.append( ROOT.TGraphErrors() )  
        cglobParamEvol[-1].SetMarkerStyle(20)
        cglobParamEvol[-1].SetName('cglobparam%d_evol'%i)
        for key in cglobParamMap:
            en=ET_PTS[key[0]]*ROOT.TMath.CosH(ETA_PTS[key[1]])
            f=cglobParamMap[key].GetFunction('cfx')
            try:
                p,perr=f.GetParameter(i),f.GetParError(i)
                np=cglobParamEvol[-1].GetN()
                cglobParamEvol[-1].SetPoint(np,en,p)
                cglobParamEvol[-1].SetPointError(np,0,perr)
            except:
                pass
        cglobParamEvol[-1].Sort()

        if cglobParamEvol[-1].GetN()>3:
            p=ROOT.TF1('cglob_p%d_vsen'%i,'[0]*pow(log(x),2)+[1]*log(x)+[2]',1,1e4)
            p.SetParName(0,'a')
            p.SetParName(1,'b')
            p.SetParName(2,'c')
            cglobParamEvol[-1].Fit(p,'RWQ+','',2.0,1.0e4)
            
    return cglobParamEvol
