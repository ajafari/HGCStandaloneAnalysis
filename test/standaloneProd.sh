#!/bin/bash

export LSB_JOB_REPORT_MAIL=N

dir=/store/cmst3/group/hgcal/Geant4/
#for model in 63 630 64 640; do # 65 650 66 660; do
for model in 65 650 66 660; do
    for p in gamma pi-; do
        for eta in 1.6 1.8 2.0 2.2; do
            commonOpts="-t FHGranularity -v ${model} -m 2 -a ${eta} -d ${p} -n 400 -e ${dir} -g --enList 2,5,7,10,15,25,35,50,100,200"
            for run in `seq 0 25`; do
                #python submitProd.py ${commonOpts} -r ${run}; 
                python submitDigi.py ${commonOpts} -s 1nw -q 1nw -o `pwd`/data -E ${dir} -r ${run} --nPuVtx 0; 
            done
        done
    done 
done 
