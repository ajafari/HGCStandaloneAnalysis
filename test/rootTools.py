import ROOT
import os
from array import array
import numpy as np
from scipy import stats
from rounding import *

def initROOT(batch=True):
    """basic ROOT customization"""    
    #ROOT.gROOT.SetBatch(batch)
    ROOT.gStyle.SetOptTitle(0)
    ROOT.gStyle.SetOptStat(0)
    ROOT.RooMsgService.instance().setSilentMode(True);
    ROOT.RooMsgService.instance().getStream(0).removeTopic(ROOT.RooFit.Minimization);
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Minimization);
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.ObjectHandling);
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.DataHandling);
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Fitting);
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Plotting);
    ROOT.RooMsgService.instance().getStream(0).removeTopic(ROOT.RooFit.InputArguments);
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.InputArguments);
    ROOT.RooMsgService.instance().getStream(0).removeTopic(ROOT.RooFit.Eval);
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Eval);
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Integration);
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.NumIntegration);
    ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.NumIntegration);

    #Viridis palette reversed + white
    stops = array('d', [0.0, 0.05, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0000])
    red   = array('d', [26./255., 51./255.,  43./255.,  33./255.,  28./255.,  35./255.,  74./255., 144./255., 246./255., 1., 1.])
    green = array('d', [9./255., 24./255.,  55./255.,  87./255., 118./255., 150./255., 180./255., 200./255., 222./255., 1., 1.])
    blue  = array('d', [30./255., 96./255., 112./255., 114./255., 112./255., 101./255.,  72./255.,  35./255.,   0./255., 1., 1.])
    ROOT.TColor.CreateGradientColorTable(11, stops, red[::-1], green[::-1], blue[::-1], 255)


def getQuantiles(data,varList):
    """get quantiles from a RooDataSet (np.percentile only available after v1.5...)"""
    dataArray=[]
    for i in xrange(0,data.numEntries()):
        datapoint =  data.get(i)
        dataArray.append( [ datapoint.getRealValue('en_'+v) for v in varList] )

    #perc=np.percentile(dataArray,[1,16,50,84,99],axis=0)

    dataQ={}
    try:
        for i in xrange(0,len(varList)):
            v=varList[i]
            dataQ[v]=[x for x in np.percentile(dataArray,[5,16,50,84,99])]
    except:
        for q in [5,16,50,84,99]:
            perc=stats.scoreatpercentile(dataArray,q)
            for i in xrange(0,len(varList)):
                v=varList[i]
                if not v in dataQ: dataQ[v]=[]
                dataQ[v].append(perc[i])

    return dataQ 
    
def getGrComparison(gr1,gr2,ratio,maxRelUnc=1.0):
    """compares two plots (difference subtracted in quadrature or ratio)"""

    compGr=ROOT.TGraphErrors()
    x,y1,y2=ROOT.Double(0),ROOT.Double(0),ROOT.Double(0)
    for i in xrange(0,gr1.GetN()):
        gr1.GetPoint(i,x,y1)
        ey1=gr1.GetErrorY(i)
        gr2.GetPoint(i,x,y2)
        ey2=gr2.GetErrorY(i)

        if ratio:
            newy=float(y2)/float(y1)
            newyUnc=ROOT.TMath.Sqrt((float(y2)*ey1)**2+(float(y1)*ey2)**2)/(float(y1)**2)
            if newy>0 and abs(newyUnc/newy) > maxRelUnc : continue
        else:
            newy=float(y2)**2-float(y1)**2
            newy=ROOT.TMath.Sqrt(newy) if newy>0 else -ROOT.TMath.Sqrt(-newy)
            newyUnc=ROOT.TMath.Sqrt((float(y2)*ey2)**2+(float(y1)*ey1)**2)/abs(newy)
        
        compGr.SetPoint(i,float(x),newy)
        compGr.SetPointError(i,0,newyUnc)
    return compGr


def showGraphCollection(grColl,outName,xtitle,ytitle,yran=None,func=None,grid=False,rightLeg=False,drawOpt='p',extraInfo=[],grColl2=None,logx=False):
    """shows a collection of graphs in a canvas with a tidy format"""

    c=ROOT.TCanvas('c','c',500,500)
    c.SetTopMargin(0.06)
    c.SetLeftMargin(0.15)
    c.SetBottomMargin(0.1)
    c.SetRightMargin(0.02)
    c.SetLogx(logx)
    
    gr=ROOT.TMultiGraph()
    legPos=(0.15,0.93,0.8,0.93-0.045*len(grColl))
    if rightLeg:
        legPos=(0.65,0.93,0.94,0.93-0.045*len(grColl))
    leg=ROOT.TLegend(legPos[0],legPos[1],legPos[2],legPos[3])

    for key in sorted(grColl.iterkeys()):

        gr.Add(grColl[key],drawOpt)

        label=grColl[key].GetTitle()
        if func:
            f=grColl[key].GetFunction(func)
            label += '#scale[0.65]{'
            try:
                for i in xrange(0,f.GetNpar()):
                    pname,pval,punc=f.GetParName(i),f.GetParameter(i),f.GetParError(i)
                    label += ' %s=%s'%(pname,toROOTRounded(pval,punc))
            except Exception,e:
                print 'Unable to get fit result'
                print e
            label += '}'
        leg.AddEntry(grColl[key],label,drawOpt)

    gr.Draw('a')
    gr.GetXaxis().SetTitleSize(0.05)
    gr.GetYaxis().SetTitleSize(0.05)
    gr.GetYaxis().SetTitleOffset(1.1)
    gr.GetXaxis().SetTitle(xtitle)
    gr.GetYaxis().SetTitle(ytitle)

    try:
        gr2=ROOT.TMultiGraph()
        for key in sorted(grColl2.iterkeys()):
            gr2.Add(grColl2[key],'p')
        gr2.Draw('p')
    except:
        pass

    if leg.GetNRows()>1:
        gr.GetYaxis().SetRangeUser(gr.GetYaxis().GetXmin(),1.2*gr.GetYaxis().GetXmax())
        leg.SetBorderSize(0)
        leg.SetFillStyle(0)
        leg.SetTextSize(0.03)
        leg.Draw()

    if yran:
        gr.GetYaxis().SetRangeUser(yran[0],yran[1])
    if grid:
        c.SetGridx()
        c.SetGridy()

    tex=ROOT.TLatex()
    tex.SetTextFont(42)
    tex.SetTextSize(0.04)
    tex.SetNDC()
    tex.DrawLatex(0.15,0.96,'#bf{CMS} #it{simulation preliminary}')
    tex.DrawLatex(0.72,0.96,'HGCAL Geant4')
    for x in extraInfo:
        tex.DrawLatex(0.2,0.9,x)

    c.Modified()
    c.Update()
    for ext in ['png','pdf','root']:
        c.SaveAs('{0}.{1}'.format(os.path.splitext(outName)[0],ext))
    c.Delete()

def getEffSigma(var,pdf,wmin=-10,wmax=10,step=0.002,epsilon=1e-4):
    """ effective  width code from H->gg analysis """
    #get cdf points
    cdf = pdf.createCdf(ROOT.RooArgSet(var))
    point=wmin;
    points=[]
    while (point <= wmax):
        var.setVal(point);
        if pdf.getVal() > epsilon :
            points.append((point,cdf.getVal()))
        point+=step

    #find the 68% CI
    low,high=wmin,wmax
    width=high-low
    for i in xrange(0,len(points)):
        for j in xrange(i,len(points)):
            wy=points[j][1]-points[i][1]
            if abs(wy-0.683)<epsilon:
                wx=points[j][0]-points[i][0]
                if wx<width:
                    low=points[i][0]
                    high=points[j][0]
                    width=wx

    return low,high

def buildTH2Profile(h,minStats=5):
    """builds the profile for a 2D histogram, requiring minimal stats in each projection"""
    gr=ROOT.TGraphErrors()
    gr.SetMarkerStyle(20)
    gr.SetName(h.GetName()+'_prof')
    for xbin in xrange(1,h.GetNbinsX()+1):
        py=h.ProjectionY('py',xbin,xbin)
        if py.Integral()<minStats: continue
        np=gr.GetN()
        gr.SetPoint(np,h.GetXaxis().GetBinCenter(xbin),py.GetMean())
        gr.SetPointError(np,0.5*h.GetXaxis().GetBinWidth(xbin),py.GetMeanError())
    return gr
