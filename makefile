# name of the library
LIBNAME = HGCStandaloneAnalysis

#Necessary to use shell built-in commands
SHELL=bash

# Define include paths
USERINCLUDES += -I$(ROOTSYS)/include/
USERINCLUDES += -I$(ROOFITSYS)/include/
USERINCLUDES += -isystem $(BOOSTSYS)/include

USERINCLUDES += -Iinclude/ -I../PFCalEE/userlib/include/ -I../PFCalEE/analysis/include -I$(FASTJET_INSTALL)/include/
USERINCLUDES += -I$(HEPMC_DIR)/include/

# Define libraries to link
USERLIBS += $(shell root-config --glibs) -lRooFit -lGenVector #-lTreePlayer -lTMVA
USERLIBS += -L../PFCalEE/userlib/lib -lPFCalEEuserlib
USERLIBS += -Wl,-rpath,$(FASTJET_INSTALL)/lib -lm  -L$(FASTJET_INSTALL)/lib -lfastjettools -lfastjet -lfastjetplugins -lsiscone_spherical -lsiscone
USERLIBS += -L$(BOOSTSYS)/lib -lboost_regex -lboost_program_options -lboost_filesystem

CXXFLAGS = -Wall -W -O2 -std=c++0x # -std=c++11 
LDFLAGS = -shared -Wall -W
CXX=g++
LD=g++

CXXFLAGS += $(USERINCLUDES)
LIBS += $(USERLIBS)

# A list of directories
BASEDIR = $(shell pwd)
LIBDIR = $(BASEDIR)/lib
EXEDIR = $(BASEDIR)/bin
SRCDIR = $(BASEDIR)/src
DOCDIR= $(BASEDIR)/docs
OBJ_EXT=o
TEST_EXT=cpp

# Build a list of srcs and bins to build
SRCS=$(wildcard $(BASEDIR)/src/*.cc)
EXES=$(wildcard $(BASEDIR)/test/*.cpp)
OBJS=$(subst $(SRCDIR), $(OBJDIR),$(subst cc,$(OBJ_EXT),$(SRCS)))

BINS=$(EXEDIR)/prepareCalibrationTree $(EXEDIR)/prepareRecoNtuple

SUBDIRS = userlib

.PHONY: $(SUBDIRS) all
all: $(BINS)

docs: all
	doxygen Doxyfile

$(SUBDIRS):
	$(MAKE) -C $@

$(EXEDIR)/prepareCalibrationTree:  $(SRCDIR)/prepareCalibrationTree.cpp $(LIBDIR)/lib$(LIBNAME).so $(wildcard $(BASEDIR)/include/*.h*)
	$(CXX) -o $@ $(CXXFLAGS) $< $(LIBS) -L$(LIBDIR) -l$(LIBNAME)
$(EXEDIR)/prepareRecoNtuple:  $(SRCDIR)/prepareRecoNtuple.cpp $(LIBDIR)/lib$(LIBNAME).so $(wildcard $(BASEDIR)/include/*.h*)
	$(CXX) -o $@ $(CXXFLAGS) $< $(LIBS) -L$(LIBDIR) -l$(LIBNAME)

$(LIBDIR)/lib$(LIBNAME).so:  $(OBJS) $(LIBDEPENDS)
	$(LD) $(LDFLAGS) -o $(LIBDIR)/lib$(LIBNAME).so $(OBJS) $(LIBS)

lib: $(LIBDIR)/lib$(LIBNAME).so

dict:
	rootcint -v -f src/dict.cc -c include/LinkDef.h
	mv src/dict.h include/dict.h

clean: 
	rm -rf $(OBJS)  $(LIBDIR)/lib$(LIBNAME).so $(BINS) 
