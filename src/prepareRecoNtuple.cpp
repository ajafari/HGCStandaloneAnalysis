#include<string>
#include<iostream>
#include<fstream>
#include<sstream>
#include<map>
#include<vector>

#include <boost/algorithm/string.hpp>
#include "boost/lexical_cast.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"
#include "boost/function.hpp"

#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH3F.h"
#include "TH2F.h"
#include "TH1F.h"
#include "TF1.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TRandom3.h"
#include "TMatrixD.h"
#include "TMatrixDSym.h"
#include "TVectorD.h"
#include "TSystemDirectory.h"
#include "TSystemFile.h"
#include "TVector2.h"

#include "HGCSSEvent.hh"
#include "HGCSSInfo.hh"
#include "HGCSSSamplingSection.hh"
#include "HGCSSSimHit.hh"
#include "HGCSSRecoHit.hh"
#include "HGCSSGenParticle.hh"
#include "HGCSSParameters.hh"
#include "HGCSSCalibration.hh"
#include "HGCSSDigitisation.hh"
#include "HGCSSDetector.hh"
#include "HGCSSGeometryConversion.hh"
#include "HGCSSPUenergy.hh"
#include "HGCSSMipHit.hh"

#include "PositionFit.hh"
#include "SignalRegion.hh"

#include "Math/Vector3D.h"
#include "Math/Vector3Dfwd.h"
#include "Math/Point2D.h"
#include "Math/Point2Dfwd.h"

#include "utilities.h"

#include "CommonTools.h"

using boost::lexical_cast;
namespace po=boost::program_options;
using namespace std;

int main(int argc, char** argv){//main  

  //
  // CONFIGURATION
  //

  //configuration parameters
  unsigned pNevts,pStartInputs,pNinputs;
  std::string inDir,outFile,recoTag,simTag,recoTreeName,simTreeName;
  unsigned debug;
  float enmin;

  //read configuration from file
  std::string cfg;
  po::options_description preconfig("Configuration"); 
  preconfig.add_options()("cfg,c",po::value<std::string>(&cfg)->required());
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(preconfig).allow_unregistered().run(), vm);
  po::notify(vm);
  po::options_description config("Configuration");
  config.add_options()
    //Input output and config options //->required()
    ("pNevts",       po::value<unsigned>(&pNevts)->default_value(0))
    ("pStartInputs", po::value<unsigned>(&pStartInputs)->default_value(0))
    ("pNinputs",     po::value<unsigned>(&pNinputs)->default_value(1))
    ("inDir",        po::value<std::string>(&inDir)->required())
    ("outFile",      po::value<std::string>(&outFile)->required())
    ("debug",        po::value<unsigned>(&debug)->default_value(0))
    ("recoTag",      po::value<std::string>(&recoTag)->required())
    ("simTag",       po::value<std::string>(&simTag)->required())
    ("recoTreeName", po::value<std::string>(&recoTreeName)->required())
    ("simTreeName",  po::value<std::string>(&simTreeName)->required())
    ("enmin",        po::value<float>(&enmin)->default_value(1.0))
    ;
  po::store(po::command_line_parser(argc, argv).options(config).allow_unregistered().run(), vm);
  po::store(po::parse_config_file<char>(cfg.c_str(), config), vm);
  po::notify(vm);

  std::cout << " -- Input parameters: " << std::endl
	    << " -- Input directory: " << inDir << std::endl
	    << " -- Output file path: " << outFile << std::endl
	    << " -- Processing ";
  if (pNevts == 0) std::cout << "all events." << std::endl;
  else             std::cout << pNevts << " events per run." << std::endl;
  if(pStartInputs>0) std::cout << "Starting at input file #" << pStartInputs << std::endl;
  if(pNinputs>0)     std::cout << "Processing " << pNinputs << " files" << std::endl;
  else               std::cout << "Processing all events" << std::endl;
  std::cout << "Reco tag: " << recoTag << " sim tag:" << simTag << std::endl;

   std::pair<TChain *,TChain *> evChain=getEventChain(recoTreeName,recoTag,simTreeName,simTag,inDir,pStartInputs,pNinputs,debug);
  TChain *reco=evChain.first;
  TChain *sim=evChain.second;
  HGCSSDetector &myDetector = getDetector(sim);
  
  //decode the tree
  HGCSSEvent * event = 0;
  std::vector<HGCSSRecoHit> * rechitvec = 0;
  std::vector<HGCSSSimHit> *simhitvec=0;
  std::vector<HGCSSGenParticle> *genpartvec=0;
  std::vector<HGCSSSamplingSection> *samplesecvec=0;
  unsigned nPuVtx = 0;
  reco->SetBranchAddress("HGCSSEvent",&event);
  reco->SetBranchAddress("HGCSSRecoHitVec",&rechitvec);
  reco->SetBranchAddress("HGCSSSimHitVec",&simhitvec);
  reco->SetBranchAddress("HGCSSGenParticleVec",&genpartvec);
  reco->SetBranchAddress("HGCSSSamplingSectionVec",&samplesecvec);
  if (reco->GetBranch("nPuVtx")) reco->SetBranchAddress("nPuVtx",&nPuVtx);
  
  //
  // PREPARE OUTPUT
  //
  TFile *outputFile = TFile::Open(outFile.c_str(),"RECREATE");
  if (!outputFile) {
    std::cout << " -- Error, output file " << outFile << " cannot be opened. Please create output directory. Exiting..." << std::endl;
    return 1;
  }
  else {
    std::cout << " -- output file " << outputFile->GetName() << " successfully opened." << std::endl;
  }
  outputFile->cd();

  TTree *t=new TTree("hgc","hgc");
  Int_t ev_run,ev_lumi,ev_event;
  float vtx_x,vtx_y,vtx_z;
  std::vector<double> genpart_eta,genpart_phi,genpart_pt,genpart_energy;
  std::vector<double> genpart_dvx,genpart_dvy,genpart_dvz;
  std::vector<double> genpart_fbrem;
  std::vector<int> genpart_pid,genpart_gen,genpart_reachedEE,genpart_fromBeamPipe;
  std::vector<std::vector<double> > genpart_posx,genpart_posy,genpart_posz;
  std::vector<double> rechit_eta,rechit_phi,rechit_pt,rechit_energy;
  std::vector<double> rechit_x,rechit_y,rechit_z,rechit_time,rechit_thickness;
  std::vector<int> rechit_layer,rechit_wafer,rechit_cell;
  std::vector<unsigned int> rechit_detid;
  std::vector<bool> rechit_isHalf;
  std::vector<int> rechit_flags,rechit_cluster2d;

  t->Branch("event", &ev_event);
  t->Branch("lumi", &ev_lumi);
  t->Branch("run", &ev_run);
  t->Branch("vtx_x", &vtx_x);
  t->Branch("vtx_y", &vtx_y);
  t->Branch("vtx_z", &vtx_z);
  
  t->Branch("genpart_eta","vector<double>", &genpart_eta);
  t->Branch("genpart_phi","vector<double>", &genpart_phi);
  t->Branch("genpart_pt","vector<double>", &genpart_pt);
  t->Branch("genpart_energy","vector<double>", &genpart_energy);
  t->Branch("genpart_dvx","vector<double>", &genpart_dvx);
  t->Branch("genpart_dvy","vector<double>", &genpart_dvy);
  t->Branch("genpart_dvz", &genpart_dvz);
  t->Branch("genpart_fbrem","vector<double>", &genpart_fbrem);
  t->Branch("genpart_pid","vector<int>", &genpart_pid);
  t->Branch("genpart_gen","vector<int>", &genpart_gen);
  t->Branch("genpart_reachedEE","vector<int>", &genpart_reachedEE);
  t->Branch("genpart_fromBeamPipe","vector<int>", &genpart_fromBeamPipe);
  t->Branch("genpart_posx","vector<vector<double> >", &genpart_posx);
  t->Branch("genpart_posy","vector<vector<double> >", &genpart_posy);
  t->Branch("genpart_posz","vector<vector<double> >", &genpart_posz);
  
  t->Branch("rechit_eta","vector<double>", &rechit_eta);
  t->Branch("rechit_phi","vector<double>", &rechit_phi);
  t->Branch("rechit_pt","vector<double>", &rechit_pt);
  t->Branch("rechit_energy","vector<double>", &rechit_energy);
  t->Branch("rechit_x","vector<double>", &rechit_x);
  t->Branch("rechit_y","vector<double>", &rechit_y);
  t->Branch("rechit_z","vector<double>", &rechit_z);
  t->Branch("rechit_time", &rechit_time);
  t->Branch("rechit_thickness","vector<double>", &rechit_thickness);
  t->Branch("rechit_layer","vector<int>", &rechit_layer);
  t->Branch("rechit_wafer","vector<int>", &rechit_wafer);
  t->Branch("rechit_cell","vector<int>", &rechit_cell);
  t->Branch("rechit_detid","vector<unsigned int>", &rechit_detid);
  t->Branch("rechit_isHalf","vector<bool>", &rechit_isHalf);
  t->Branch("rechit_flags","vector<int>", &rechit_flags);
  t->Branch("rechit_cluster2d","vector<int>", &rechit_cluster2d);

  t->SetDirectory(outputFile);

  //
  // LOOP OVER EVENTS
  //
  std::vector<double> nullVec;
  const unsigned nEvts = ((pNevts > reco->GetEntries() || pNevts==0) ? static_cast<unsigned>(reco->GetEntries()) : pNevts) ;
  for (unsigned ievt(0); ievt<nEvts; ++ievt) {

    reco->GetEntry(ievt);
    if (debug)             std::cout << "... Processing entry: " << ievt << std::endl;
    else if (ievt%50 == 0) std::cout << "... Processing entry: " << ievt << std::endl;
    
    //event header
    ev_run=1;
    ev_lumi=0;
    ev_event=event->eventNumber();
    vtx_x=event->vtx_x();
    vtx_y=event->vtx_y();
    vtx_z=event->vtx_z();

    //reset info
    genpart_pid.clear();
    genpart_energy.clear();   
    genpart_pid.clear();
    genpart_energy.clear();
    genpart_eta.clear();
    genpart_phi.clear();
    genpart_pt.clear();
    genpart_dvx.clear();
    genpart_dvy.clear();
    genpart_dvz.clear();
    genpart_fbrem.clear();
    genpart_gen.clear();
    genpart_reachedEE.clear();
    genpart_fromBeamPipe.clear();
    genpart_posx.clear();
    genpart_posy.clear();
    genpart_posz.clear();
    rechit_energy.clear();
    rechit_pt.clear();
    rechit_eta.clear();
    rechit_phi.clear();
    rechit_x.clear();
    rechit_y.clear();
    rechit_z.clear();
    rechit_time.clear();

    //store gen particle information
    for(auto g : *genpartvec) {
      genpart_pid.push_back( g.pdgid() );
      genpart_energy.push_back( g.E() );
      genpart_eta.push_back( g.eta() );
      genpart_phi.push_back( g.phi() );
      genpart_pt.push_back( g.pt() );
      genpart_dvx.push_back( g.x() );
      genpart_dvy.push_back( g.y() );
      genpart_dvz.push_back( g.z() );
      genpart_fbrem.push_back( 0. );
      genpart_gen.push_back( 0. );
      genpart_reachedEE.push_back( g.trackID() );
      genpart_fromBeamPipe.push_back( g.isIncoming() );
      genpart_posx.push_back( nullVec );
      genpart_posy.push_back( nullVec );
      genpart_posz.push_back( nullVec );
    }

    //store rec hits
    for (auto hit : *rechitvec ) {
      if(hit.E()<enmin) continue;
      rechit_energy.push_back( hit.E() );
      rechit_pt.push_back( hit.pt() );
      rechit_eta.push_back( hit.eta() );
      rechit_phi.push_back( hit.phi() );
      rechit_x.push_back( hit.get_x() );
      rechit_y.push_back( hit.get_y() );
      rechit_z.push_back( hit.get_z() );
      rechit_time.push_back( hit.time() );

      const HGCSSSubDetector & subdet = myDetector.subDetectorByLayer(hit.layer());      
      rechit_layer.push_back( hit.layer() );
      rechit_wafer.push_back( 0 );
      rechit_cell.push_back( subdet.isSi );
      rechit_thickness.push_back( 200. );
      rechit_detid.push_back( 0 );
      rechit_isHalf.push_back( false );
      rechit_flags.push_back( 0 );
      rechit_cluster2d.push_back( 0 );
    }
  
    //fill tree
    t->Fill();    
  }

  //write results to file  
  outputFile->cd();
  t->Write();
  outputFile->Close();

  return 0; 
}
