#include<string>
#include<iostream>
#include<fstream>
#include<sstream>
#include<map>
#include <boost/algorithm/string.hpp>
#include "boost/lexical_cast.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"
#include "boost/function.hpp"

#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH3F.h"
#include "TH2F.h"
#include "TH1F.h"
#include "TF1.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TRandom3.h"
#include "TMatrixD.h"
#include "TMatrixDSym.h"
#include "TVectorD.h"
#include "TSystemDirectory.h"
#include "TSystemFile.h"
#include "TVector2.h"
#include "TRandom2.h"

#include "HGCSSEvent.hh"
#include "HGCSSInfo.hh"
#include "HGCSSSamplingSection.hh"
#include "HGCSSSimHit.hh"
#include "HGCSSRecoHit.hh"
#include "HGCSSGenParticle.hh"
#include "HGCSSParameters.hh"
#include "HGCSSCalibration.hh"
#include "HGCSSDigitisation.hh"
#include "HGCSSDetector.hh"
#include "HGCSSGeometryConversion.hh"
#include "HGCSSPUenergy.hh"
#include "HGCSSMipHit.hh"

#include "PositionFit.hh"
#include "SignalRegion.hh"

#include "Math/Vector3D.h"
#include "Math/Vector3Dfwd.h"
#include "Math/Point2D.h"
#include "Math/Point2Dfwd.h"

#include "utilities.h"

#include "fastjet/ClusterSequence.hh"

#include "CommonTools.h"

#define MAXHITS 5000

using boost::lexical_cast;
namespace po=boost::program_options;
using namespace std;
using namespace fastjet;

int main(int argc, char** argv){//main  

  TRandom2 rand;

  //
  // CONFIGURATION
  //

  //configuration parameters
  unsigned pNevts,pStartInputs,pNinputs;
  std::string inDir,outFile,recoTag,simTag,recoTreeName,simTreeName;
  unsigned debug;
  float enmin,dRwindow;

  //read configuration from file
  std::string cfg;
  po::options_description preconfig("Configuration"); 
  preconfig.add_options()("cfg,c",po::value<std::string>(&cfg)->required());
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(preconfig).allow_unregistered().run(), vm);
  po::notify(vm);
  po::options_description config("Configuration");
  config.add_options()
    //Input output and config options //->required()
    ("pNevts",       po::value<unsigned>(&pNevts)->default_value(0))
    ("pStartInputs", po::value<unsigned>(&pStartInputs)->default_value(0))
    ("pNinputs",     po::value<unsigned>(&pNinputs)->default_value(1))
    ("inDir",        po::value<std::string>(&inDir)->required())
    ("outFile",      po::value<std::string>(&outFile)->required())
    ("debug",        po::value<unsigned>(&debug)->default_value(0))
    ("recoTag",      po::value<std::string>(&recoTag)->required())
    ("simTag",       po::value<std::string>(&simTag)->required())
    ("recoTreeName", po::value<std::string>(&recoTreeName)->required())
    ("simTreeName",  po::value<std::string>(&simTreeName)->required())
    ("enmin",        po::value<float>(&enmin)->default_value(1.0))
    ("dRwindow",     po::value<float>(&dRwindow)->default_value(0.5))
    ;
  po::store(po::command_line_parser(argc, argv).options(config).allow_unregistered().run(), vm);
  po::store(po::parse_config_file<char>(cfg.c_str(), config), vm);
  po::notify(vm);

  std::cout << " -- Input parameters: " << std::endl
	    << " -- Input directory: " << inDir << std::endl
	    << " -- Output file path: " << outFile << std::endl
	    << " -- Processing ";
  if (pNevts == 0) std::cout << "all events." << std::endl;
  else             std::cout << pNevts << " events per run." << std::endl;
  if(pStartInputs>0) std::cout << "Starting at input file #" << pStartInputs << std::endl;
  if(pNinputs>0)     std::cout << "Processing " << pNinputs << " files" << std::endl;
  std::cout << "Reco tag: " << recoTag << " sim tag:" << simTag << std::endl;

  std::pair<TChain *,TChain *> evChain=getEventChain(recoTreeName,recoTag,simTreeName,simTag,inDir,pStartInputs,pNinputs,debug);
  TChain *reco=evChain.first;
  TChain *sim=evChain.second;
  HGCSSDetector &myDetector = getDetector(sim);
  const Int_t nLayers=myDetector.nLayers();
  
  //decode the tree
  HGCSSEvent * event = 0;
  std::vector<HGCSSRecoHit> * rechitvec = 0;
  std::vector<HGCSSSimHit> *simhitvec=0;
  std::vector<HGCSSGenParticle> *genpartvec=0;
  std::vector<HGCSSSamplingSection> *samplesecvec=0;
  unsigned nPuVtx = 0;
  reco->SetBranchAddress("HGCSSEvent",&event);
  reco->SetBranchAddress("HGCSSRecoHitVec",&rechitvec);
  reco->SetBranchAddress("HGCSSSimHitVec",&simhitvec);
  reco->SetBranchAddress("HGCSSGenParticleVec",&genpartvec);
  reco->SetBranchAddress("HGCSSSamplingSectionVec",&samplesecvec);
  if (reco->GetBranch("nPuVtx")) reco->SetBranchAddress("nPuVtx",&nPuVtx);
  
  //
  // PREPARE OUTPUT
  //
  TFile *outputFile = TFile::Open(outFile.c_str(),"RECREATE");
  if (!outputFile) {
    std::cout << " -- Error, output file " << outFile << " cannot be opened. Please create output directory. Exiting..." << std::endl;
    return 1;
  }
  else {
    std::cout << " -- output file " << outputFile->GetName() << " successfully opened." << std::endl;
  }
  outputFile->cd();

  std::vector<std::vector<float> > sectionInfo(nLayers+1);
  TTree *hitTree=new TTree("hits","hits"); 
  Int_t genId;
  Float_t genEn,genEt,genEta,genPhi;
  Int_t nhits;
  Bool_t  hit_si[MAXHITS];
  Int_t   hit_lay[MAXHITS];
  Float_t hit_x[MAXHITS],hit_y[MAXHITS],hit_z[MAXHITS],hit_en[MAXHITS],hit_dR[MAXHITS],hit_dRho[MAXHITS];
  Float_t clustSumEn[5][100];
  Float_t si_sumen[nLayers],sci_sumen[nLayers];
  Float_t si_sumen_5ic[nLayers], sci_sumen_5ic[nLayers];
  Float_t si_sumen_10ic[nLayers],sci_sumen_10ic[nLayers];
  Float_t si_sumen_15ic[nLayers],sci_sumen_15ic[nLayers];
  Float_t si_sim_sumen[nLayers],sci_sim_sumen[nLayers];
  hitTree->Branch("genId",  &genId,   "genId/I");
  hitTree->Branch("genEn",  &genEn,   "genEn/F");
  hitTree->Branch("genEt",  &genEt,   "genEt/F");
  hitTree->Branch("genEta", &genEta,  "genEta/F");
  hitTree->Branch("genPhi", &genPhi,  "genPhi/F");
  hitTree->Branch("nhits",  &nhits,   "nhits/I");
  hitTree->Branch("hit_si",  hit_si,  "hit_si[nhits]/O");
  hitTree->Branch("hit_lay", hit_lay, "hit_lay[nhits]/I");
  hitTree->Branch("hit_x",   hit_x,   "hit_x[nhits]/F");
  hitTree->Branch("hit_y",   hit_y,   "hit_y[nhits]/F");
  hitTree->Branch("hit_z",   hit_z,   "hit_z[nhits]/F");
  hitTree->Branch("hit_en",  hit_en,  "hit_en[nhits]/F");
  hitTree->Branch("hit_dR",  hit_dR,  "hit_dR[nhits]/F");
  hitTree->Branch("hit_dRho", hit_dRho, "hit_dRho[nhits]/F");
  hitTree->Branch("clustSumEn", clustSumEn, "clustSumEn[5][100]/F");
  hitTree->Branch("si_sumen", si_sumen, Form("si_sumen[%d]/F",nLayers));
  hitTree->Branch("sci_sumen", sci_sumen, Form("sci_sumen[%d]/F",nLayers));
  hitTree->Branch("si_sumen_5ic",   si_sumen_5ic,   Form("si_sumen_5ic[%d]/F",nLayers));
  hitTree->Branch("sci_sumen_5ic",  sci_sumen_5ic,  Form("sci_sumen_5ic[%d]/F",nLayers));
  hitTree->Branch("si_sumen_10ic",  si_sumen_10ic,  Form("si_sumen_10ic[%d]/F",nLayers));
  hitTree->Branch("sci_sumen_10ic", sci_sumen_10ic, Form("sci_sumen_10ic[%d]/F",nLayers));
  hitTree->Branch("si_sumen_15ic",  si_sumen_15ic,  Form("si_sumen_15ic[%d]/F",nLayers));
  hitTree->Branch("sci_sumen_15ic", sci_sumen_15ic, Form("sci_sumen_15ic[%d]/F",nLayers));
  hitTree->Branch("si_sim_sumen", si_sim_sumen, Form("si_sim_sumen[%d]/F",nLayers));
  hitTree->Branch("sci_sim_sumen", sci_sim_sumen, Form("sci_sim_sumen[%d]/F",nLayers));

  hitTree->SetAutoFlush(1000);
  hitTree->SetDirectory(outputFile);

  //
  // LOOP OVER EVENTS
  //
  const unsigned nEvts = ((pNevts > reco->GetEntries() || pNevts==0) ? static_cast<unsigned>(reco->GetEntries()) : pNevts) ;
  for (unsigned ievt(0); ievt<nEvts; ++ievt) {

    reco->GetEntry(ievt);
    if (debug)             std::cout << "... Processing entry: " << ievt << std::endl;
    else if (ievt%50 == 0) std::cout << "... Processing entry: " << ievt << std::endl;

    //require one single generated particle hitting the detector
    if(genpartvec->size()!=1) continue;
    genId=(*genpartvec)[0].pdgid();
    genEn=(*genpartvec)[0].E();
    genEt=int(1e-3*sqrt(pow((*genpartvec)[0].mass(),2)+pow((*genpartvec)[0].pt(),2)));
    genEta=float(int((*genpartvec)[0].eta()*10))*0.1;
    genPhi=(*genpartvec)[0].phi();

    //update section info
    for(auto sec : *samplesecvec) 
      {
        int volNb(sec.volNb()); 
        std::vector<float> info(3,0);
        info[0]=sec.volX0trans();
        info[1]=sec.volLambdatrans();
        info[2]=sec.voldEdx();
        sectionInfo[volNb]=info;
      }

    //reset energy sums and hit counts
    nhits=0;
    for(Int_t ilay=0; ilay<nLayers; ilay++) { 

      //silicon
      si_sim_sumen[ilay]=0;       
      si_sumen[ilay]=0; 
      si_sumen_5ic[ilay]=0;
      si_sumen_10ic[ilay]=0;
      si_sumen_15ic[ilay]=0;
      
      //scintillator
      sci_sim_sumen[ilay]=0;
      sci_sumen[ilay]=0;
      sci_sumen_5ic[ilay]=0;
      sci_sumen_10ic[ilay]=0;
      sci_sumen_15ic[ilay]=0;
    }

    //integrate sim hits
    for (auto hit : *simhitvec ) {
      const HGCSSSubDetector & subdet = myDetector.subDetectorByLayer(hit.layer());
      if(subdet.isScint)
        sci_sim_sumen[hit.layer()]+=hit.energy();
      else
        si_sim_sumen[hit.layer()]+=hit.energy();
    }

    //integrate rec hits and store more granular information on recHits around region of interest
    std::vector<PseudoJet> pseudoParticles;
    for (auto hit : *rechitvec ) {
      //apply some fiducial cuts
      float en(hit.energy());
      if(en<enmin)  continue;

      //save as pseudo-jet for the clustering
      PseudoJet ip=PseudoJet(hit.px(),hit.py(),hit.pz(),hit.energy());
      ip.set_user_index( hit.layer() );
      pseudoParticles.push_back( ip );

      //select in a deltaR window
      float eta(hit.eta()),phi(hit.phi());
      float dR=sqrt( pow(eta-genEta,2)+pow(TVector2::Phi_mpi_pi(phi-genPhi),2) );
      if(dR>dRwindow) continue;
      
      //cartesian distance
      float rhoCen = hit.get_z()/TMath::SinH(genEta);
      float xcen   = rhoCen*TMath::Cos(genPhi);
      float ycen   = rhoCen*TMath::Sin(genPhi);
      float dRho   = TMath::Sqrt( TMath::Power(hit.get_x()-xcen,2)+TMath::Power(hit.get_y()-ycen,2) );

      Int_t ilay=hit.layer();
      const HGCSSSubDetector & subdet = myDetector.subDetectorByLayer(hit.layer());      
      if(subdet.isScint)
        {
          sci_sumen[ilay]      += en;
          sci_sumen_5ic[ilay]  += max(0.,rand.Gaus(0,0.05*en)+en);
          sci_sumen_10ic[ilay] += max(0.,rand.Gaus(0,0.10*en)+en);
          sci_sumen_15ic[ilay] += max(0.,rand.Gaus(0,0.15*en)+en);
        }
      else
        {
          si_sumen[ilay]      += en;
          si_sumen_5ic[ilay]  += max(0.,rand.Gaus(0,0.05*en)+en);
          si_sumen_10ic[ilay] += max(0.,rand.Gaus(0,0.10*en)+en);
          si_sumen_15ic[ilay] += max(0.,rand.Gaus(0,0.15*en)+en);
        }

      if(nhits<MAXHITS)
        {
          hit_lay[nhits] = ilay;
          hit_x[nhits]   = hit.get_x();
          hit_y[nhits]   = hit.get_y();        
          hit_z[nhits]   = hit.get_z();
          hit_en[nhits]  = en;
          hit_dR[nhits]  = dR;
          hit_dRho[nhits] = dRho;
          hit_si[nhits]  = !subdet.isScint;
          nhits++;
        }
    }
    
    //run fast jet on rec hits and save energy clustered in each layer
    float R[5]={0.1,0.2,0.3,0.5};
    for(size_t ir=0; ir<5; ir++)
      {
        for(int ilay=0; ilay<100; ilay++) clustSumEn[ir][ilay]=0;

        JetDefinition jet_def(antikt_algorithm, R[ir]);
        ClusterSequence cs(pseudoParticles, jet_def);
        std::vector<PseudoJet> jets = sorted_by_pt(cs.inclusive_jets());
        
        //get closest in direction
        float minDR(9999);
        int jidx(-1);
        for(size_t j=0; j<jets.size(); j++)
          {
            float dR=sqrt( pow(jets[j].eta()-genEta,2)+pow(TVector2::Phi_mpi_pi(jets[j].phi()-genPhi),2));
            if(dR>minDR) continue;
            minDR=dR;
            jidx=j;
          }

        if(jidx>=0)
          {
            for(auto jconst :  jets[jidx].constituents())
              {
                int ilay=jconst.user_index();
                clustSumEn[ir][ilay] += jconst.E();
              }
          }
      }

    if(nhits==0) continue;
    hitTree->Fill();
  }

  //write results to file  
  outputFile->cd();
  TH1F *x0H     = new TH1F("x0",";Section;X_{0}",      sectionInfo.size(),0,sectionInfo.size());
  TH1F *lambdaH = new TH1F("lambda",";Section;#lambda",sectionInfo.size(),0,sectionInfo.size());
  TH1F *dedxH   = new TH1F("dedx",";Section;dE/dx",    sectionInfo.size(),0,sectionInfo.size());
  for(size_t volNb=0; volNb<sectionInfo.size(); volNb++)
    {
      std::vector<float> &info=sectionInfo[volNb];
      if(info.size()<2) continue;
      x0H->Fill(volNb,info[0]);
      lambdaH->Fill(volNb,info[1]);
      dedxH->Fill(volNb,info[2]);
    }
  dedxH->Write();
  x0H->Write();
  lambdaH->Write();
  hitTree->Write();
  outputFile->Close();

  return 0; 
}
